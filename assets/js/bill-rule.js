$(function (){
    var bill_rule = $("#bill-rule").DataTable({
        'dom':'i<"right"l>rtp',   // l - length changing input control
                        // f - filtering input
                        // t - The table!
                        // i - Table information summary
                        // p - pagination control
                        // r - processing display element
      //    "processing": true,                 //加载数据时显示正在加载信息
            // "serverSide": true,
        "iDisplaylength":10,
        "sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        },
        // "ajax":{
        //  // "url":"assets/js/scripts/post.php",
        //  // "type":"POST",
        // },
        'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'className': 'dt-body-center',
         'render': function (data, type, full, meta){
             return '<input type="checkbox">';
            }
        }],
        'order': [1, 'asc'],
    });

    // $.buttonGroupInit('staff-manage', true, true, true, true);
    $(".operation-button").buttonGroupAction({
        tableId: 'bill-rule',
        table: bill_rule,
        ajaxUrl: '/home/getRepairJson',
        buttonImportExcel: false,
        buttonDelete: true,
        buttonSelectAll: true,
        buttonAdd: true
    })

    //  新增

    $('button[rel="add-bill-rule"]').click(function (){
        layer.open({
            type: 2,
            title: ['新增账单规则', 'font-size:18px;font-family:Lato,sans-serif;text-align:center'],
            shadeClose: true,
            shade: 0.3,
            closeBtn: 2, 
            area: ['500px', '383px'],
            content: 'bill_rule/bill_rule_add' //iframe的url
        });  
    });

    //  查阅
    $('button[for="bill-rule-details"]').click(function (){
        layer.open({
            type: 2,
            title: ['账单规则详情', 'font-size:18px;font-family:Lato,sans-serif;text-align:center'],
            shadeClose: true,
            shade: 0.3,
            closeBtn: 2, 
            area: ['500px', '383px'],
            content: 'bill_rule/bill_rule_details' //iframe的url
        });  
    });
});

