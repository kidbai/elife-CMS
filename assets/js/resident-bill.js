function format ( d ) {
    // `d` is the original data object for the row
    var tmp = '<tr>' +
                 '<td>{billType}</td>' +
                 '<td>{standar}</td>' +
                 '<td>{month}</td>' +
                 '<td>{count}</td>' +
                 '<td>{subtotal}</td>' +
                 '<td>{billStatus}</td>' +
                 '<td>{remark}</td>' +
              '</tr>';

    var tmp2 = '';
    var tableItem = '';
    for(var i = 0;i < d.billType.length;i++)
    {
        var status = '';
        if(d.billStatus[i] === "未缴" )
        {
            var status = '<label class="label label-danger">{status}</label>';
            status = status.replace(/{status}/, d.billStatus[i]);
            tmp2 = tmp.replace(/{month}/, d.month[i]).replace(/{billType}/, d.billType[i]).replace(/{standar}/, d.standar[i])
            .replace(/{count}/, d.count[i]).replace(/{subtotal}/, d.subtotal[i]).replace(/{billStatus}/, status);
        }
        else
        {
            tmp2 = tmp.replace(/{month}/, d.month[i]).replace(/{billType}/, d.billType[i]).replace(/{standar}/, d.standar[i])
            .replace(/{count}/, d.count[i]).replace(/{subtotal}/, d.subtotal[i]).replace(/{billStatus}/, d.billStatus[i]);
        }

        if(d.remark[i].indexOf('欠费') === 0)
        { 
            var remark = '<label class="label label-danger">{remark}</label>';
            remark = remark.replace(/{remark}/, d.remark[i]);
            tmp2 = tmp2.replace(/{remark}/, remark);
        }

        tableItem += tmp2;
    }
    var tpl =   '<table class="table table-bordered second-level" cellpadding="5" cellspacing="0" style="padding-left:50px;">'+
                    '<thead>' + 
                        '<tr>' +
                            '<th>费用类型</th>' +
                            '<th>收费标准</th>' +
                            '<th>数量(月)</th>' +
                            '<th>到期时间</th>' +
                            '<th>小计</th>' +
                            '<th>缴费状态</th>' +
                            '<th>备注</th>' +
                        '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                        '{tableItem}' +
                    '</tbody>' +
                '</table>';
    tpl = tpl.replace(/{tableItem}/, tableItem);
    return tpl;



}

$(function (){
    var resident_bill = $("#resident-bill").DataTable({
        'dom':'i<"right"l>rtp',   // l - length changing input control
                        // f - filtering input
                        // t - The table!
                        // i - Table information summary
                        // p - pagination control
                        // r - processing display element
        // "processing": true,                 //加载数据时显示正在加载信息
        // "serverSide": true,
        "iDisplaylength":10,
        "sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        },
        "ajax":{
         "url":"assets/js/resident-bill.json",
        },
        "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { "data": "month" },
        ],
        "columnDefs": [ {
            "targets": 1,
            "data": "month",
            "render": function ( data, type, full, meta ) {
                return data;
            }
        }],
        'order': [1, 'asc'],
        initComplete:initToolbar,
        "drawCallback": function( settings ) {
            var tr = $('#resident-bill tbody td.details-control').parent('tr');
            console.log(tr);
            // // console.log(tr);
            var row = resident_bill.row( tr );
            // console.log(row.data());
            // row.child( format(row.data()) ).show();
            // tr.addClass('shown');
        }
    });

    $('#resident-bill tbody').on('click', 'td.details-control', function () {
        console.log(this);
        var tr = $(this).closest('tr');
        console.log(tr);
        var row = resident_bill.row( tr );
        console.log(row);
 
        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });

    function initToolbar()
    {
        // 全局搜索
        var globalSearch = {};
        globalSearch['1'] = '房号';
        globalSearch['2'] = '业主姓名';
        globalSearch['3'] = '业主手机号';

        $(".toolbar").toolbarInit({
            tableId: 'resident-bill',
            table: resident_bill,
            toolbarSearch: false,
            toolbarGlobalSeach: globalSearch,
            toolbarDateTimePicker: false,
            ajaxUrl: '/home/getRepairJson'
        })

        // $.each($("#resident-bill tbody tr"), function(index, val) {
        //     var cells = $(val).find('td');
        //     // console.log(cells);
        //     console.log($(cells)[0]);
        //     console.log($(cells)[1]);
        //     // $(cells[1]).remove(); // for example I want to remove cell 1
        //     // $(cells[0]).attr('colspan','2'); // for example I want to set colspan = 2 for cell 0 
        // });
        
        var title = $(".navbar-brand").text();
        
        if( $("input:eq(3)").val() != '' )
        {
            $("button.bill-charge").removeAttr('disabled');
        }
        else
        {
            $("button.bill-charge").attr('disabled','disabled');
        }

        $(".bill-charge").click(function (){
            layer.open({
                type: 2,
                title: ['' + title + '收费单', 'font-size:18px;font-family:Lato,sans-serif;text-align:center'],
                shadeClose: true,
                shade: 0.3,
                closeBtn: 2, 
                area: ['800px', '383px'],
                content: 'resident_bill/bill_pay' //iframe的url
            });  
        });
    }

    


});

