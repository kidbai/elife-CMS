$(function (){
    var department_manage = $("#department-manage").DataTable({
        'dom':'i<"right"l>rtp',   // l - length changing input control
                        // f - filtering input
                        // t - The table!
                        // i - Table information summary
                        // p - pagination control
                        // r - processing display element
      //    "processing": true,                 //加载数据时显示正在加载信息
            // "serverSide": true,
            "iDisplaylength":10,
            "sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        },
        // "ajax":{
        //  // "url":"assets/js/scripts/post.php",
        //  // "type":"POST",
        // },
        'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'className': 'dt-body-center',
         'render': function (data, type, full, meta){
             return '<input type="checkbox">';
            }
        }],
        'order': [1, 'asc'],
    });

    // 全局搜索
    var globalSearch = {};
    globalSearch['1'] = '房号';
    globalSearch['2'] = '业主姓名';

    $(".operation-button").buttonGroupAction({
        tableId: 'department-manage',
        table: department_manage,
        ajaxUrl: '/home/getRepairJson',
        buttonImportExcel: false,
        buttonDelete: true,
        buttonSelectAll: true,
        buttonAdd: true
    });

    $(".toolbar").toolbarInit({
        tableId: 'department-manage',
        table: department_manage,
        toolbarSearch: false,
        toolbarGlobalSeach: false,
        toolbarDateTimePicker: false,
        ajaxUrl: '/home/getRepairJson'
    });

    
    //  新增

    $('button[rel="add-department-manage"]').click(function (){
        layer.open({
            type: 2,
            title: ['新增部门', 'font-size:18px;font-family:Lato,sans-serif;text-align:center'],
            shadeClose: true,
            shade: 0.3,
            closeBtn: 2, 
            area: ['500px', '340px'],
            content: 'department/department_manage_add' //iframe的url
        });  
    });

    //  查阅
    $('button[for="department-manage-details"]').click(function (){
        layer.open({
            type: 2,
            title: ['部门详情', 'font-size:18px;font-family:Lato,sans-serif;text-align:center'],
            shadeClose: true,
            shade: 0.3,
            closeBtn: 2, 
            area: ['500px', '340px'],
            content: 'department/department_manage_details' //iframe的url
        });  
    });
});

