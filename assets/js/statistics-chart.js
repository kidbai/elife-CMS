function initChart(chartID)
{
    var chart = $("#" + chartID)[0];
    require.config({
        paths: {
            echarts: 'assets/echarts'
        }
    });
    // 使用
    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line',
            'echarts/chart/funnel'
        ],
        function (ec) {
            // 基于准备好的dom，初始化echarts图表
            console.log('init');
            var echart = ec.init(chart); 
            
            var echart_option = {
                title: {
                    text: 'XX费缴费情况'
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data:['物业费', '水电费', '停车费']
                },
                toolbox: {
                    show: true,
                    orient: 'horizontal',
                    x: 'right',
                    y: 'bottom',
                    color : ['#1e90ff','#22bb22','#4b0082','#d2691e'],
                    backgroundColor:'rgba(0,0,0,0)',
                    borderColor: '#ccc',
                    borderWidth: 0,            
                    padding: 5,                
                    showTitle: true,
                    feature : {
                        mark : {show: true},
                        // dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                calculable : false,
                xAxis : [
                    {
                        type : 'category',
                        data : ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月']
                    }
                ],
                yAxis : [
                    {
                        type : 'value'
                    }
                ],
                series : [
                    {
                        "name" : "物业费",
                        "type" : "bar",
                        "data" : [1000, 500, 300, 200, 300 ,500, 1000, 500, 300, 200, 300 ,500],
                        "markPoint" : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        }
                    },
                    {
                        "name" : "水电费",
                        "type" : "bar",
                        "data" : [500, 400, 200, 300 ,500 ,800, 500, 400, 200, 300 ,500 ,800],
                        "markPoint" : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        }
                    },
                    {
                        "name" : "停车费",
                        "type" : "bar",
                        "data" : [400, 200, 750, 300, 250, 120, 400, 200, 750, 300, 250, 120],
                        "markPoint" : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        }
                    }
                ]
            };

    
            // 为echarts对象加载数据 
            echart.setOption(echart_option); 
        }
    );
}

$(function (){
    initChart('chart-1');
});

$(window).onload = function (){
    initChart('chart-2');
    console.log('done');
}