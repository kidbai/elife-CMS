$(function (){
    var role = $("#role").DataTable({
        'dom':'i<"right"l>rtp',   // l - length changing input control
                        // f - filtering input
                        // t - The table!
                        // i - Table information summary
                        // p - pagination control
                        // r - processing display element
      //    "processing": true,                 //加载数据时显示正在加载信息
            // "serverSide": true,
            "iDisplaylength":10,
            "sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        },
        // "ajax":{
        //  // "url":"assets/js/scripts/post.php",
        //  // "type":"POST",
        // },
        'order': [1, 'asc'],
        'columnDefs': [{
            'targets': 0,
            'searchable':false,
            'orderable':false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                return '<input type="checkbox">';
            }
        }],
        'order': [1, 'asc'],
    });
    

    // 全局搜索
    var globalSearch = {};
    globalSearch['roomId'] = '房号';
    globalSearch['userNo'] = '用户编号';
    globalSearch['userPhone'] = '用户手机号';

    // $.buttonGroupInit('staff-manage', true, true, true, true);
    $(".operation-button").buttonGroupAction({
        tableId: 'role',
        table: role,
        ajaxUrl: '/home/getRepairJson',
        buttonImportExcel: false,
        buttonDelete: true,
        buttonSelectAll: true,
        buttonAdd: true 
    })

    //  新增

    $('button[rel="add-role"]').click(function (){
        layer.open({
            type: 2,
            title: ['新增角色', 'font-size:18px;font-family:Lato,sans-serif;text-align:center'],
            shadeClose: true,
            shade: 0.3,
            closeBtn: 2, 
            area: ['500px', '340px'],
            content: 'role/role_add' //iframe的url
        });  
    });

    //  查阅
    $('button[for="role-edit"]').click(function (){
        layer.open({
            type: 2,
            title: ['角色编辑', 'font-size:18px;font-family:Lato,sans-serif;text-align:center'],
            shadeClose: true,
            shade: 0.3,
            closeBtn: 2, 
            area: ['500px', '340px'],
            content: 'role/role_edit' //iframe的url
        });  
    });

});

