$(function (){
    var public_monitoring = $("#public-monitoring").DataTable({
        'dom':'i<"right"l>rtp',   // l - length changing input control
                        // f - filtering input
                        // t - The table!
                        // i - Table information summary
                        // p - pagination control
                        // r - processing display element
      //    "processing": true,                 //加载数据时显示正在加载信息
            // "serverSide": true,
            "iDisplaylength":10,
            "sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        },
        // "ajax":{
        //  // "url":"assets/js/scripts/post.php",
        //  // "type":"POST",
        // },
        'order': [1, 'asc'],
    });
    

    // 初始化筛选框参数 searchArrayItem
    var searchArray = [];
    var searchArrayItemLabel = {};
    var searchArrayItem_1 = {};
    searchArrayItem_1['all'] = '全部';
    searchArrayItem_1['wait'] = '待处理';
    searchArrayItem_1['done'] = '已完成';
    searchArrayItem_1['processing'] = '处理中';
    searchArrayItem_1['invalid'] = '无效';

    var searchArrayItem_2 = {};
    searchArrayItem_2['all'] = '全部';
    searchArrayItem_2['equipment'] = '设备';
    searchArrayItem_2['people'] = '人员';
    searchArrayItem_2['environment'] = '环境';
    searchArrayItem_2['other'] = '其他';


    searchArrayItemLabel['监察状态'] = searchArrayItem_1;
    searchArrayItemLabel['监察类型'] = searchArrayItem_2;
    searchArray.push(searchArrayItemLabel);

    // 全局搜索
    var globalSearch = {};
    globalSearch['roomId'] = '房号';
    globalSearch['userNo'] = '用户编号';
    globalSearch['userPhone'] = '用户手机号码';

    $(".toolbar").toolbarInit({
        tableId: 'public-monitoring',
        table: public_monitoring,
        toolbarSearch: searchArray,
        toolbarGlobalSeach: globalSearch,
        toolbarDateTimePicker: true,
        ajaxUrl: '/home/getRepairJson'
    });

    //  查阅
    $('button[for="public-monitoring-details"]').click(function (){
        layer.open({
            type: 2,
            title: ['公共监察详情', 'font-size:18px;font-family:Lato,sans-serif;text-align:center'],
            shadeClose: true,
            shade: 0.3,
            closeBtn: 2, 
            area: ['700px', '600px'],
            content: 'public_monitoring/public_monitoring_details' //iframe的url
        });  
    });

});

