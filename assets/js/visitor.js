$(function (){
    var visitor = $("#visitor").DataTable({
        'dom':'i<"right"l>rtp',   // l - length changing input control
                        // f - filtering input
                        // t - The table!
                        // i - Table information summary
                        // p - pagination control
                        // r - processing display element
      //    "processing": true,                 //加载数据时显示正在加载信息
            // "serverSide": true,
            "iDisplaylength":10,
            "sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        },
        // "ajax":{
        //  // "url":"assets/js/scripts/post.php",
        //  // "type":"POST",
        // },
        'order': [1, 'asc'],
    });
    

    // 初始化筛选框参数 searchArrayItem
    var searchArray = [];
    var searchArrayItemLabel = {};
    var searchArrayItem = {};
    searchArrayItem['submit'] = '已提交';
    searchArrayItem['done'] = '已完成';
    searchArrayItem['undo'] = '已撤销';

    searchArrayItemLabel['到访状态'] = searchArrayItem;
    searchArray.push(searchArrayItemLabel);

    // 全局搜索
    var globalSearch = {};
    globalSearch['roomId'] = '房号';
    globalSearch['visitorName'] = '访客姓名';
    globalSearch['visitorPhone'] = '访客电话';

    $(".toolbar").toolbarInit({
        tableId: 'visitor',
        table: visitor,
        toolbarSearch: searchArray,
        toolbarGlobalSeach: globalSearch,
        toolbarDateTimePicker: true,
        ajaxUrl: '/home/getRepairJson'
    })

});

