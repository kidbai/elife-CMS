$(function (){

    $('button[for="department-manage-add"]').click(function (){
        console.log($("form.dialog").serialize());
        $.ajax({
            url: '',
            type: 'post',
            dataType: 'json',
            data: $("form.dialog").serialize(),
            success: function (data)
            {
                console.log(data);
            }
        });
    });

    $('button[for="department-position-add"]').click(function (){
        var count = $(".department-position").length;
        var tpl = ' <div class="form-group department-position">' +
                        '<label>部门职位:</label>' +
                        '<input name="{departmentPosition}" type="text" class="form-control">' +
                    '</div>';

        count++;
        var departmentPosition = 'departmentPosition-' + count;

        tpl = tpl.replace(/{departmentPosition}/, departmentPosition);

        $(".department-position:last").after(tpl);
    });

    $('.department-position').delegate('.glyphicon-remove-sign', 'click', function (){
        $(this).parent().parent().remove();
    });

    $('.label-success.label-plus').click(function (){
        var tpl =   '<div class="label label-info mr-5">' +
                      '<div class="label-content inline-block" contenteditable="true">请输入职位</div>'+
                      '<a><i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i></a>' +
                    '</div>';
        $('.label-info:last').after(tpl);
    });

    $('button[for="department-manage-modify"]').modify('department-manage');
    $('button[for="department-manage-modify"]').click(function (){
        $('.label-plus').addClass('on');
    });
    $('button[for="department-manage-modify-cancel"]').cancel('department-manage');
     $('button[for="department-manage-modify-cancel"]').click(function (){
        $('.label-plus').removeClass('on');
     });
    $('button[for="department-manage-del"]').dialogDel();
    $('button[for="department-manage-modify-submit"]').click(function (){
        console.log('click');
        var data = [];
        $.each($(".dialog input"), function(index, inputItem) {
            var item = {};
            console.log(inputItem);
            $inputItem = $(inputItem);
            // console.log(inputItem.attr('name'));
            item[$inputItem.attr('name')] = $inputItem.val();
            data.push(item);
        });
        var departmentPosition = {};
        var departmentPositionArray = [];
        $.each($(".department-position .label-content"), function(index, el) {
            console.log(el);
            $el = $(el);
            departmentPositionArray.push($el.text());
        });
        departmentPosition['department-position'] = departmentPositionArray;
        data.push(departmentPosition);

        console.log(data);

        $.ajax({
            url: '',
            type: 'post',
            dataType: 'json',
            data: { data: data },
            success: function (data)
            {

            }
        })
        
        
    });
});