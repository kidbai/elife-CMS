$(function (){
    var repair = $("#repair").DataTable({
        'dom':'i<"right"l>rtp',   // l - length changing input control
                        // f - filtering input
                        // t - The table!
                        // i - Table information summary
                        // p - pagination control
                        // r - processing display element
      //    "processing": true,                 //加载数据时显示正在加载信息
            // "serverSide": true,
            "iDisplaylength":10,
            "sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        },
        // "ajax":{
        //  // "url":"assets/js/scripts/post.php",
        //  // "type":"POST",
        // },
        'order': [1, 'asc'],
    });
    

    // 初始化筛选框参数 searchArrayItem
    var searchArray = [];
    var searchArrayItemLabel = {};
    var searchArrayItem = {};
    searchArrayItem['wait'] = '待处理';
    searchArrayItem['processing'] = '处理中';
    searchArrayItem['done'] = '已完成';
    searchArrayItem['invalid'] = '无效';

    searchArrayItemLabel['报修状态'] = searchArrayItem;
    searchArray.push(searchArrayItemLabel);

    // 全局搜索
    var globalSearch = {};
    globalSearch['roomId'] = '房号';
    globalSearch['appUser'] = '维修人';
    globalSearch['appUserPhone'] = '维修人电话';
    globalSearch['appUserNo'] = '保修单';

    $(".operation-button").buttonGroupAction({
        tableId: 'repair',
        table: repair,
        ajaxUrl: '/home/getRepairJson',
        buttonImportExcel: false,
        buttonDelete: false,
        buttonSelectAll: false,
        buttonAdd: true
    })

    $(".toolbar").toolbarInit({
        tableId: 'repair',
        table: repair,
        toolbarSearch: searchArray,
        toolbarGlobalSeach: globalSearch,
        toolbarDateTimePicker: true,
        ajaxUrl: '/home/getRepairJson'
    })

    //  查阅
    $('button[rel="add-repair"]').click(function (){
        layer.open({
            type: 2,
            title: ['新增业主报修', 'font-size:18px;font-family:Lato,sans-serif;text-align:center'],
            shadeClose: true,
            shade: 0.3,
            closeBtn: 2, 
            area: ['500px', '640px'],
            content: 'repair/repair_add' //iframe的url
        });  
    });

    //  查阅
    $('button[for="repair-details"]').click(function (){
        layer.open({
            type: 2,
            title: ['业主报修详情', 'font-size:18px;font-family:Lato,sans-serif;text-align:center'],
            shadeClose: true,
            shade: 0.3,
            closeBtn: 2, 
            area: ['700px', '600px'],
            content: 'repair/repair_details' //iframe的url
        });  
    });
});

