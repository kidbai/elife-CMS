function format ( d ) {
    // `d` is the original data object for the row
    var tmp = '<tr>' +
                 '<td>{billType}</td>' +
                 '<td>{standar}</td>' +
                 '<td>{month}</td>' +
                 '<td>{endDate}</td>' +
                 '<td>{subtotal}</td>' +
              '</tr>';

    var tmp2 = '';
    var tableItem = '';
    for(var i = 0;i < d.billType.length;i++)
    {
        tmp2 = tmp.replace(/{billType}/, d.billType[i]).replace(/{standar}/, d.standar).replace(/{month}/, d.month)
        .replace(/{endDate}/, d.endDate).replace(/{subtotal}/, d.subtotal);
        tableItem += tmp2;
    }
    var tpl =   '<table class="table table-bordered second-level" cellpadding="5" cellspacing="0" style="padding-left:50px;">'+
                    '<thead>' + 
                        '<tr>' +
                            '<th>费用类型</th>' +
                            '<th>收费标准(元/月)</th>' +
                            '<th>数量(月)</th>' +
                            '<th>到期时间</th>' +
                            '<th>小计</th>' +
                        '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                        '{tableItem}' +
                    '</tbody>' +
                '</table>';
    tpl = tpl.replace(/{tableItem}/, tableItem);
    return tpl;



}

$(function (){
    var bill_detail = $("#bill-detail").DataTable({
        'dom':'i<"right"l>rtp',   // l - length changing input control
                        // f - filtering input
                        // t - The table!
                        // i - Table information summary
                        // p - pagination control
                        // r - processing display element
        "processing": true,                 //加载数据时显示正在加载信息
        "serverSide": true,
        "iDisplaylength":10,
        "sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        },
        "ajax":{
            "url":"assets/js/bill.json",
            "type":"POST",
            "data": function ( d ) {
                $.getToolBarValue(d);
            }
        },
        "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { "data": "orderNo" },
            { "data": "roomId" },
            { "data": "total"},
            { "data": "way"},
            { "data": "payer"},
            { "data": "payTime"}
            // { "data": "position" },
            // { "data": "office" },
            // { "data": "start_date" },
            // { "data": "salary" }
        ],
        "columnDefs": [ {
            "targets": 1,
            "data": "first_name",
            "render": function ( data, type, full, meta ) {
                return data;
            }
        }],
        'order': [1, 'asc'],
        initComplete:initToolbar
    });

    $('#bill-detail tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = bill_detail.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );

    function initToolbar()
    {
        // 初始化筛选框参数 searchArrayItem
        var searchArray = [];
        var searchArrayItemLabel = {};
        var searchArrayItemType = {};
        searchArrayItemType['all'] = '全部';
        searchArrayItemType['propertyFee'] = '物业费';
        searchArrayItemType['cleanFee'] = '清洁费';
        searchArrayItemType['carPortFee'] = '机动车停车费';

        var searchArrayItemWay = {};
        searchArrayItemWay['all'] = '全部';
        searchArrayItemWay['online'] = '线上';
        searchArrayItemWay['offline'] = '线下';


        searchArrayItemLabel['缴费类型%bill_type'] = searchArrayItemType;
        searchArrayItemLabel['缴费途径%bill_way'] = searchArrayItemWay;
        searchArray.push(searchArrayItemLabel);

        // 全局搜索
        var globalSearch = {};
        globalSearch['roomId'] = '房号';
        globalSearch['orderNo'] = '订单号';

        $(".toolbar").toolbarInit({
            tableId: 'bill-detail',
            table: bill_detail,
            toolbarSearch: searchArray,
            toolbarGlobalSeach: globalSearch,
            toolbarDateTimePicker: true,
            ajaxUrl: '/home/getRepairJson'
        })
    }

});


