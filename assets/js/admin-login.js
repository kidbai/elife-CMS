function login() {
    var username = $('input[name="username"]').val();
    var password = $('input[name="password"]').val();
    var validateCode = $('input[name="validatecode"]').val();
    if(username == '')
    {
        $(".alert").addClass('on');
        $(".alert").text("请输入账号");
        return;
    }
    if(password == '')
    {
        $(".alert").addClass('on');
        $(".alert").text("请输入密码")
        return;
    }
    if(validateCode == '')
    {
        $(".alert").addClass('on');
        $(".alert").text('请输入验证码');
        return;
    }
    $.ajax({
        url: 'propLogin/loader',
        type: 'post',
        data: $(".login-form").serialize(),
        success: function ( data )
        {
            console.log(data);
            if(data == 2)
            {
                $(".alert").addClass('on');
                $(".alert").text('账号密码错误，请重新输入正确的账号密码');
            }
            else if( data == 1 )
            {
                window.location.href = '/home/dashboard';
            }
            if(data == 3)
            {
                $(".alert").addClass('on');
                $(".alert").text('验证码错误');
            }
        }
    })
}

$(function (){
    $("input").focus(function (){
        $(".alert").removeClass('on');
    });

    $("#login").click(function (){
        login();
    });

    $(window).keydown(function (ev){
        if(ev.keyCode === 13)
        {
            login();
        }
    });
});