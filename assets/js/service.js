$(function (){
    var service = $("#service").DataTable({
        'dom':'i<"right"l>rtp',   // l - length changing input control
                        // f - filtering input
                        // t - The table!
                        // i - Table information summary
                        // p - pagination control
                        // r - processing display element
      //    "processing": true,                 //加载数据时显示正在加载信息
            // "serverSide": true,
            "iDisplaylength":10,
            "sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        },
        // "ajax":{
        //  // "url":"assets/js/scripts/post.php",
        //  // "type":"POST",
        // },
        'order': [1, 'asc'],
        'columnDefs': [{
            'targets': 0,
            'searchable':false,
            'orderable':false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                return '<input type="checkbox">';
            }
        }],
        'order': [1, 'asc'],
    });
    

    // 初始化筛选框参数 searchArrayItem
    var searchArray = [];
    var searchArrayItemLabel = {};
    var searchArrayItem = {};
    searchArrayItem['all'] = '安保部';
    searchArrayItem['announcement'] = '客服部';
    searchArrayItem['news'] = '行政部';

    searchArrayItemLabel['部门'] = searchArrayItem;
    searchArray.push(searchArrayItemLabel);

    // 全局搜索
    var globalSearch = {};
    globalSearch['num'] = '工号';
    globalSearch['name'] = '姓名';

    $(".toolbar").toolbarInit({
        tableId: 'service',
        table: service,
        toolbarSearch: false,
        toolbarGlobalSeach: globalSearch,
        toolbarDateTimePicker: false,
        ajaxUrl: '/home/getRepairJson'
    });

});

