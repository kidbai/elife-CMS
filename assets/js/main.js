
$(function() {
    $.pjax({
        selector: 'a.pjaxTrigger',
        container: '#content-wrapper', //内容替换的容器
        show: 'fade',  //展现的动画，支持默认和fade, 可以自定义动画方式，这里为自定义的function即可。
        cache: false,  //是否使用缓存
        storage: false,  //是否使用本地存储
        titleSuffix: '', //标题后缀
        filter: function(href){
            if(href === 'home/statistics_chart')
            {
                return true;
            }
        },
        callback: function(status){
            var type = status.type;
            switch(type){
                case 'success': console.log('success');break; //正常
                case 'cache': console.log('cahce');break; //读取缓存 
                case 'error': console.error('error');break; //发生异常
                case 'hash': ;break; //只是hash变化
            }   
        }
    });


    $("#side-menu > li > a").click(function (){
        $(".nav-second-level").hide();
        $("#side-menu > li").removeClass('active');
        $(this).parent().addClass('active');
        if($(this).siblings('.nav-second-level').length > 0)
        {
            // $(this).siblings(".nav-second-level").toggle(300);
            $(this).siblings(".nav-second-level").toggle(300);
            $(this).children('.fa-angle-down').toggleClass('active');
        }
        if($(this).parent().attr('position') == '0')
        {
            var sidebarPosition = []; // 记录导航栏位置
            sidebarPosition.push($(this).parent().attr('position'));
            cookieTool.setCookie('sidebarPosition', sidebarPosition, 10);
        }
    });
    

    $("#side-menu .nav-second-level li a").click(function (){
        $("#side-menu > li").removeClass('active');
        var sidebarPosition = []; // 记录导航栏位置
        $("#side-menu .nav-second-level li a").removeClass('active');
        $(this).addClass('active');
        var positionOneLevel = $(this).parent().parent().parent().attr('position');
        sidebarPosition.push(positionOneLevel);
        sidebarPosition.push($(this).attr('position'));
        cookieTool.setCookie('sidebarPosition', sidebarPosition, 10);
    });

    $("#side-menu li > a").click(function(event) {
        event.preventDefault();
    });

    // 加载cookie 
    var cookie_array = cookieTool.getCookie('sidebarPosition').split(','); 
    if(cookie_array.length < 2)
    {
        $("#menu li").each(function (){
            // console.log($(this).attr('rel'));
            if($(this).attr('position') === cookie_array[0])
            {
                $(this).addClass('active');
            }
        });
    }
    else
    {
        $("#menu li").each(function (){
            if($(this).attr('position') === cookie_array[0])
            {
                $(this).children('.nav-second-level').toggle(300);
                $(this).addClass('active');
                $(this).children('.nav-second-level').children('li').children('a').each(function (){
                    if($(this).attr('position') === cookie_array[1])
                    {
                        $(this).addClass('active');
                    }
                }); 
            }
        });
    }
});