$(function (){
    $('#entryDate').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0,
        showMeridian: 1,
        pickerPosition: "bottom-left"
        // endDate: true,
    });

    $('#leavingDate').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0,
        showMeridian: 1,
        pickerPosition: "bottom-left",
        // endDate: true,
    });

    var url = "assets/php/";
    $('#fileupload').fileupload({
        add: function(e, data) {
            var uploadErrors = [];
            var acceptFileTypes = /^image\/(jpe?g)$/i;
            if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                uploadErrors.push('图片格式不对');
            }
            if(data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 5000000) {
                uploadErrors.push('文件过大');
            }
            if(uploadErrors.length > 0) {
                alert(uploadErrors.join("\n"));
            } else {
                data.submit();
            }
        },
        url: url,
        dataType: 'json',
        // autoUpload: true,
        // acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        // maxFileSize: 5000000,
        done: function (e, data) {
            $("#progress").removeClass('display-none');
            $.each(data.result.files, function (index, file) {
                console.log(file.name);
                $('<p/>').text('上传成功').appendTo('#files');
                $("#staff-avatar").attr("src", "assets/php/files/"+file.name);
                $('input[name="file-name"]').val(file.name);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    //  表单提交
    $('button[for="staff-manage-add"]').addDialog('/ajax/aaaadd');

    //  查阅修改
    $(".buttonDetailsActions").buttonDetailsActions('staff-manage', '/ajax/ad');
    

});