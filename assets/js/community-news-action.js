$(function (){

    var editor = KindEditor.create("#doc_content", {height: '400px'});

    var url = "/assets/php/";
    $('#fileupload').fileupload({
        add: function(e, data) {
            var uploadErrors = [];
            var acceptFileTypes = /^image\/(jpe?g)$/i;
            if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                uploadErrors.push('图片格式不对');
            }
            if(data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 5000000) {
                uploadErrors.push('文件过大');
            }
            if(uploadErrors.length > 0) {
                alert(uploadErrors.join("\n"));
            } else {
                data.submit();
            }
        },
        url: url,
        dataType: 'json',
        // autoUpload: true,
        // acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        // maxFileSize: 5000000,
        done: function (e, data) {
            $("#progress").removeClass('display-none');
            $.each(data.result.files, function (index, file) {
                console.log(file.name);
                $('<p/>').text('上传成功').appendTo('#files');
                $("#staff-avatar").attr("src", "/assets/php/files/"+file.name);
                $('input[name="community-news-top-pic"]').val(file.name);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    // var editor = new Simditor({
    //     textarea: $('#doc_content')
    //     //optional options
    // });

    $('button[for="community-news-submit"]').click(function (){
        $('textarea[name="doc_content"]').val(editor.html());
        console.log($("form").serialize());  // data
        $.ajax({
            url: '',
            type: '',
            dataType: '',
            data: {param1: 'value1'},
            success: function (data)
            {

            }
        });
        
    });

    

});