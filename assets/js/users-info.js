$(function (){
    var users_info = $("#users-info").DataTable({
        'dom':'i<"right"l>rtp',   // l - length changing input control
                        // f - filtering input
                        // t - The table!
                        // i - Table information summary
                        // p - pagination control
                        // r - processing display element
      //    "processing": true,                 //加载数据时显示正在加载信息
            // "serverSide": true,
            "iDisplaylength":10,
            "sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        },
        // "ajax":{
        //  // "url":"assets/js/scripts/post.php",
        //  // "type":"POST",
        // },
        'order': [1, 'asc'],
    });
    

    // 初始化筛选框参数 searchArrayItem
    var searchArray = [];
    var searchArrayItemLabel = {};
    var searchArrayItem = {};
    searchArrayItem['roomId'] = '房间号';
    searchArrayItem['orderNo'] = '保修单';
    searchArrayItem['appUser'] = '报修人';
    searchArrayItem['appUser-phone'] = '报修人电话';

    var users_status = {};
    users_status['all'] = '全部';
    users_status['active'] = '激活';

    var carport_status = {};
    carport_status['use'] = '在用';
    carport_status['none'] = '无';

    searchArrayItemLabel['用户角色'] = searchArrayItem;
    searchArrayItemLabel['用户状态'] = users_status;
    searchArrayItemLabel['车位状态'] = carport_status;
    searchArray.push(searchArrayItemLabel);

    // 全局搜索
    var globalSearch = {};
    globalSearch['userNum'] = '用户编号';
    globalSearch['roomId'] = '房号';
    globalSearch['userName'] = '姓名';
    globalSearch['userPhone'] = '手机号码';

    $(".toolbar").toolbarInit({
        tableId: 'users-info',
        table: users_info,
        toolbarSearch: searchArray,
        toolbarGlobalSeach: globalSearch,
        toolbarDateTimePicker: true,
        ajaxUrl: '/home/getRepairJson'
    })

    //  查阅
    $('button[for="users-info-details"]').click(function (){
        layer.open({
            type: 2,
            title: ['用户信息', 'font-size:18px;font-family:Lato,sans-serif;text-align:center'],
            shadeClose: true,
            shade: 0.3,
            closeBtn: 2, 
            area: ['500px', '430px'],
            content: 'users/users_info_details' //iframe的url
        });  
    });
});

