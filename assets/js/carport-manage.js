$(function (){
    var carport_manage = $("#carport-manage").DataTable({
        'dom':'i<"right"l>rtp',   // l - length changing input control
                        // f - filtering input
                        // t - The table!
                        // i - Table information summary
                        // p - pagination control
                        // r - processing display element
      //    "processing": true,                 //加载数据时显示正在加载信息
            // "serverSide": true,
        "iDisplaylength":10,
        "sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        },
        // "ajax":{
        //  // "url":"assets/js/scripts/post.php",
        //  // "type":"POST",
        // },
        'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'className': 'dt-body-center',
         'render': function (data, type, full, meta){
             return '<input type="checkbox">';
            }
        }],
        'order': [1, 'asc'],
    });
    

    // 初始化筛选框参数 searchArrayItem
    var searchArray = [];
    var searchArrayItemLabel = {};
    var searchArrayItem = {};
    searchArrayItem['xianzhi'] = '闲置';
    searchArrayItem['zulin'] = '租赁';
    searchArrayItem['chushou'] = '出售';

    searchArrayItemLabel['车辆状态'] = searchArrayItem;
    searchArray.push(searchArrayItemLabel);

    // 全局搜索
    var globalSearch = {};
    globalSearch['1'] = '车位号';
    globalSearch['2'] = '车牌号';

    // $.buttonGroupInit('staff-manage', true, true, true, true);
    $(".operation-button").buttonGroupAction({
        tableId: 'carport-manage',
        table: carport_manage,
        ajaxUrl: '/home/getRepairJson',
        buttonImportExcel: true,
        buttonDelete: true,
        buttonSelectAll: true,
        buttonAdd: true
    })

    $(".toolbar").toolbarInit({
        tableId: 'carport-manage',
        table: carport_manage,
        toolbarSearch: searchArray,
        toolbarGlobalSeach: globalSearch,
        toolbarDateTimePicker: true,
        ajaxUrl: '/home/getRepairJson'
    })

    //  新增

    $('button[rel="add-carport-manage"]').click(function (){
        layer.open({
            type: 2,
            title: ['新增车位信息', 'font-size:18px;font-family:Lato,sans-serif;text-align:center'],
            shadeClose: true,
            shade: 0.3,
            closeBtn: 2, 
            area: ['500px', '430px'],
            content: 'carport/carport_manage_add' //iframe的url
        });  
    });

    //  查阅
    $('button[for="carport-manage-details"]').click(function (){
        layer.open({
            type: 2,
            title: ['车辆信息详情', 'font-size:18px;font-family:Lato,sans-serif;text-align:center'],
            shadeClose: true,
            shade: 0.3,
            closeBtn: 2, 
            area: ['500px', '430px'],
            content: 'carport/carport_manage_details' //iframe的url
        });  
    });
});

