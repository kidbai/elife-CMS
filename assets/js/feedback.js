$(function (){
    var feedback = $("#feedback").DataTable({
        'dom':'i<"right"l>rtp',   // l - length changing input control
                        // f - filtering input
                        // t - The table!
                        // i - Table information summary
                        // p - pagination control
                        // r - processing display element
      //    "processing": true,                 //加载数据时显示正在加载信息
            // "serverSide": true,
            "iDisplaylength":10,
            "sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        },
        // "ajax":{
        //  // "url":"assets/js/scripts/post.php",
        //  // "type":"POST",
        // },
        'order': [1, 'asc'],
        'columnDefs': [{
            'targets': 0,
            'searchable':false,
            'orderable':false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                return '<input type="checkbox">';
            }
        }],
        'order': [1, 'asc'],
    });
    

    // 全局搜索
    var globalSearch = {};
    globalSearch['roomId'] = '房号';
    globalSearch['userNo'] = '用户编号';
    globalSearch['userPhone'] = '用户手机号';

    $(".toolbar").toolbarInit({
        tableId: 'feedback',
        table: feedback,
        toolbarSearch: false,
        toolbarGlobalSeach: globalSearch,
        toolbarDateTimePicker: false,
        ajaxUrl: '/home/getRepairJson'
    });

    //  查阅
    $('button[for="feedback-details"]').click(function (){
        layer.open({
            type: 2,
            title: ['反馈建议详情', 'font-size:18px;font-family:Lato,sans-serif;text-align:center'],
            shadeClose: true,
            shade: 0.3,
            closeBtn: 2, 
            area: ['700px', '600px'],
            content: 'feedback/feedback_details' //iframe的url
        });  
    });

});

