$.extend({

    //  要求: 删除按钮 需要有rel属性，需要和table的id一致
    //  Demo 
    //  <button class="btn btn-danger disabled" rel="tb-feedback">删除</button>
    //  <table id="tb-feedback" class="display responsive nowrap table table-striped"> 
    buttonGroupAction: function(tableId, table ,ajaxUrl)
    {
        $("#" + tableId + " tbody").on('click', 'input[type="checkbox"]', function() {
            if($(this).is(':checked')) 
            {
                $(this).parent().parent().addClass('selected');
            }
            else 
            {
                $(this).parent().parent().removeClass('selected');
            }
            if($("#" + tableId + " tbody tr").hasClass('selected'))
            {
                $('button[rel="' + tableId + '-del' + "\"" + ']').removeClass("disabled");
            }
            else 
            {
                $('button[rel="' + tableId + '-del' + "\"" + ']').addClass("disabled");
            }
        }); 

        $('[rel="' + tableId + '-del' + '"]').click(function (){
            var delTh = $('table th:eq(2)').text();
            var delTd = $('table tr.selected td:eq(2)').text();
            console.log(delTd);
            BootstrapDialog.show({
                title: '系统提示',
                message: '确认删除<strong style="font-size: 14px; color:#e51c23">' + delTh + '</strong>是<strong style="font-size: 14px; color:#e51c23">' + delTd + '</strong>的数据?',
                type: 'type-danger',
                buttons: [{
                    label: '取消',
                    action: function(dialog){
                        dialog.close();
                    }
                }, {
                    label: '确认',
                    cssClass: 'btn-danger',
                    action: function(dialog){
                        // console.log("Del");
                        // console.log(table.rows('.selected'));
                        console.log(table.rows('.selected').data()[0]); //被选择的数据(array)
                        // console.log(table.rows());
                        console.log(table.rows('.selected').data()); //被选择的数据(array)
                        // table.row('.selected').remove().draw( false ); 
                        console.log(table.rows('.selected'));
                        dialog.close();
                        var data = table.rows('.selected').data();
                        var orderNoArray = new Array();
                        $.each(data, function() {
                            // console.log($(this)[0]['orderNo']);
                            orderNoArray.push($(this)[0]['orderNo']);
                            // console.log(orderNoArray);
                        });
                        $.ajax({
                            url: ajaxUrl,
                            type: 'POST',
                            dataType: 'json',
                            data: { orderNoArray : orderNoArray },
                            success: function (data) {
                                // console.log(data);
                                if(data)
                                {
                                    BootstrapDialog.show({
                                        title: '结果',
                                        type: 'type-success',
                                        message: '删除成功'
                                    });
                                    table.ajax.reload();
                                }
                                else
                                {
                                    BootstrapDialog.show({
                                        title: '结果',
                                        type: 'type-danger',
                                        message: '删除失败'
                                    });
                                }
                            }
                        });
                    }
                }]
            });
        });

        $('[rel="' + tableId + "-select-all" + '"]').click(function (){
            var checkBoxs = $("#" + tableId + ' tbody tr td input[type="checkbox"]');
            if($("#" + tableId + " tbody tr").hasClass('selected'))
            {
                checkBoxs.prop('checked', false);
                $("#" + tableId + " tbody tr").removeClass('selected');
                $(this).text('全选');
            }
            else
            {
                checkBoxs.prop('checked', true);
                $("#" + tableId + " tbody tr").addClass('selected');
                $(this).text('反选');
            }
        });
    },
    
    // 获取toolbar里的参数值
    getToolBarValue: function ( d )
    {
        var selectData = {};
        $("select.searchNormal").each(function(index, el) {
            $el = $(el);
            var key = $el.siblings('label').attr('for');
            if( $(this).val() != 'all' )
            {
                var value = $(this).val();
                selectData[key] = value;
            }
        });
        var globalData = {};
        if( $(".toolbar-item.globalSearch input").val() != '' )
        {
            globalData[$(".toolbar-item.globalSearch select").val()] = $(".toolbar-item.globalSearch input").val();
            d.globalData = globalData;
        }

        var dateData = {};
        if( $(".date input:eq(0)").val() != '' && $(".date input:eq(1)").val())
        {
            dateData['startDate'] = $(".date input:eq(0)").val();
            dateData['endDate'] = $(".date input:eq(1)").val();
            d.dateData = dateData;
        }

        d.select_data = selectData;
    },

    //  初始化行号
    initTableRowNumber: function ( table )
    {
        table.column(0).nodes().each( function (cell, index) {
            $cell = $(cell);
            index += table.page.info()['start'] + 1;
            $cell.text(index);
        });
    }

});

$.fn.extend({
    dialogDel: function () {
        this.click(function (){
            layer.confirm('确认删除？', {
                btn: ['确认','取消'], //按钮
                shade: false //不显示遮罩
            }, function(){
                console.log($('form.dialog input').attr('name'));
                console.log($('form.dialog input').val());
                $.ajax({
                    url: '',
                    type: 'post',
                    dataType: 'json',
                    data: {},
                })
                layer.msg('已删除', {time:1000});
            }, function(index){
                layer.close(index);
            });
        });
    },
    cancel: function ( tableId ) {
        this.click(function(event) {
            $("form select").attr('disabled', 'disabled');
            $("form input").attr('disabled', 'disabled');
            $("form textarea").attr('disabled', 'disabled');
            $(this).addClass('hidden');
            $('button[for="' + tableId + '-modify-submit"]').addClass('hidden');
            $('button[for="' + tableId + '-del"]').removeClass('hidden');
            $('button[for="' + tableId + '-modify"]').removeClass('hidden');
        });
    },
    modify: function ( tableId ) {
        this.click(function(event) {
            $("form select").removeAttr('disabled');
            $("form input").removeAttr('disabled');
            $("form textarea").removeAttr('disabled');
            $('button[for="' + tableId + '-modify-cancel"]').removeClass('hidden');
            $('button[for="' + tableId + '-modify-submit"]').removeClass('hidden');
            $('button[for="' + tableId + '-del"]').addClass('hidden');
            $(this).addClass('hidden');
        });
    },
    submitDialog: function ( ajaxUrl ) {
        this.click(function(event) {
            console.log($("form.dialog").serialize());
            // $.ajax({
            //     url: '',
            //     type: 'post',
            //     dataType: 'json',
            //     data: $("form.dialog").serialize(),
            // })
            
            layer.msg(ajaxUrl, {time:1000});
        });
    },
    addDialog: function ( ajaxUrl ) {
        this.click(function(event) {
            console.log($("form.dialog").serialize());
            layer.msg(ajaxUrl, {time:1000});
            console.log(ajaxUrl);
            // $.ajax({
            //     url: '',
            //     type: 'post',
            //     dataType: 'json',
            //     data: $("form.dialog").serialize(),
            //     success: function (data)
            //     {

            //     }
            // });
        });
    },
    buttonDetailsActions: function ( module, ajaxUrl ) {
        var tpl =   '<button type="button" class="btn btn-warning hidden mr-5" for="{module}-modify-cancel">取消</button>' +
                    '<button type="button" class="btn btn-primary hidden mr-5" for="{module}-modify-submit">确认</button>' +
                    '<button type="button" class="btn btn-danger mr-5" for="{module}-del">删除</button>' +
                    '<button type="button" class="btn btn-primary mr-5" for="{module}-modify">修改</button>';

        tpl = tpl.replace(/{module}/, module).replace(/{module}/, module).replace(/{module}/, module).replace(/{module}/, module);

        this.append(tpl);

        $('button[for="'+ module + '-modify"]').modify(module);
        $('button[for="'+ module + '-modify-cancel"]').cancel(module);
        $('button[for="'+ module + '-del"]').dialogDel();
        $('button[for="'+ module + '-modify-submit"]').submitDialog(ajaxUrl);
    }

});

$.fn.buttonGroupAction = function ( options ) {
    var options = $.extend({
        tableId: null,
        table: null, 
        ajaxUrl: null,
        buttonImportExcel: false,
        buttonDelete: true,
        buttonSelectAll: true,
        buttonAdd: true
    }, options)

    if( options.tableId === '' && options.tableId === null )
    {
        console.error('tableId is not defined');
    }

    if( options.table != null && typeof(options.table) === 'object' )
    {
    }

    if( options.ajaxUrl === '' && opions.ajaxUrl === null )
    {
        console.error('ajaxUrl is no defined');
    }

    if( options.buttonImportExcel )
    {
        var tpl =   '<button class="btn btn-default">' +
                        '导入Excel' +
                    '</button>';
        $(this).append(tpl);
    }
    if( options.buttonDelete )
    {
        var tpl =   '<button class="btn btn-danger ml-5 disabled" rel="{tableId}-del">' +
                        '删除' +
                    '</button>';
        tpl = tpl.replace(/{tableId}/, options.tableId);
        $(this).append(tpl);
    }
    if( options.buttonSelectAll )
    {
        var tpl =   '<button class="btn btn-info ml-5" rel="{tableId}-select-all">' +
                        '全选' +
                    '</button>';
        tpl = tpl.replace(/{tableId}/, options.tableId);
        $(this).append(tpl);
    }
    if( options.buttonAdd )
    {
        var tpl =   '<button class="btn btn-success ml-5" rel="add-{tableId}">' +
                        '新增' +
                    '</button>';
        tpl = tpl.replace(/{tableId}/, options.tableId);
        $(this).append(tpl);
    }
    $.buttonGroupAction(options.tableId, options.table, options.ajaxUrl);
}

$.fn.toolbarInit = function ( options ) {
    var options = $.extend({
        tableId: null,
        table: null, 
        toolbarSearch: null,
        toolbarGlobalSeach: null, 
        toolbarDateTimePicker: false,
        ajaxUrl: null
    }, options);

    if( options.tableId === null && options.tableId === '' )
    {
        console.error('tableId is not defined');
    }

    if( options.ajaxUrl === '' && opions.ajaxUrl === null )
    {
        console.error('ajaxUrl is no defined');
    }

    var template = '';

    //  搜索框模板  -- start -- 
    var searchTpl = '<div class="toolbar-item">' +
                            '<label for="{labelType}">{searchLabel}</label>' +
                            '<select class="form-control searchNormal">' +
                                '{searchOption}' + 
                            '</select>' +
                    '</div>'; 
    if( options.toolbarSearch )
    {
        template += '{toolbarSelect}';
        var searchTplItem = '';
        $.each(options.toolbarSearch, function(index, el) {
            $.each(el, function(index, el) {
                var searchTplOption = '';
                $.each(el, function(index, el) {
                    var tpl = '<option value="{key}">{value}</option>'
                    searchTplOption += tpl.replace(/{key}/, index).replace(/{value}/, el);
                });
                var labelData = index.split('%');
                labelData[0] += ':';
                searchTplItem += searchTpl.replace(/{searchLabel}/, labelData[0]).replace(/{labelType}/, labelData[1]).replace(/{searchOption}/, searchTplOption);
            });
        });
        template = template.replace(/{toolbarSelect}/, searchTplItem)
    }
    // -- end --

    //  全局搜索 -- start -- 

    var globalTpl = '<div class="toolbar-item globalSearch">' +
                        '<label>全局搜索:</label>' +
                        '<select class="form-control">' +
                            '{globalSearchOption}' + 
                        '</select>' +
                        '<input type="text" class="form-control global">' +
                        '<button class="btn btn-primary global-search">搜索</button>' +
                    '</div>';
    if( options.toolbarGlobalSeach )
    {
        template += '{globalSeach}';
        var globalSearchOption = '';
        $.each(options.toolbarGlobalSeach, function(index, el) {
            var tpl = '<option value="{key}">{value}</option>';
            globalSearchOption += tpl.replace(/{key}/, index).replace(/{value}/, el);
        });

        globalTpl = globalTpl.replace(/{globalSearchOption}/, globalSearchOption);
        template = template.replace(/{globalSeach}/, globalTpl);
    }
    // -- end --

    if( options.toolbarDateTimePicker )
    {
        template += '{dateTimePicker}';
        var dateTimePickerTpl = '<div class="clear"></div>' +
                                '<div class="toolbar-item">' +
                                    '<label>起止日期:</label>' +
                                    '<div class="form-group">' +
                                        '<div class="input-group date" id="search-startDate" data-date-format="yyyy-MM-dd">' +
                                            '<input class="form-control" value="" size="" type="text" rel="startDate" readonly>' +
                                            '<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>' +
                                            '<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="form-group">' +
                                        '<div class="input-group date" id="search-endDate" data-date-format="yyyy-MM-dd">' +
                                            '<input class="form-control" value="" size="" type="text" rel="endDate" readonly>' +
                                            '<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>' +
                                            '<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>' +
                                       '</div>' +
                                    '</div>' +
                                '</div>';
        template = template.replace(/{dateTimePicker}/, dateTimePickerTpl);
    } 

    $(this).append(template);


    if( options.toolbarDateTimePicker )
    {
    //  日期控件初始化
        var startDate = $('#search-startDate').datetimepicker({
            language:  'zh-CN',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            showMeridian: 1,
            pickerPosition: "bottom-left"
            // endDate: true,
        }).on("changeDate", function(ev){
            if(ev.date != null)
            {
                startDate=new Date(ev.date.getFullYear(),ev.date.getMonth(),ev.date.getDate(),0,0,0);
                if(endDate!=null&&endDate!='undefined'){
                    if(endDate<startDate){
                        BootstrapDialog.show({
                            title: "错误!",
                            message: "结束日期小于开始日期",
                            type: "type-danger",
                            buttons: [{
                                label: '关闭',
                                cssClass: 'btn-default',
                                action: function(dialog) {
                                    dialog.close();
                                    console.log(ev.date);
                                    ev.date = '';
                                    console.log(ev.date);
                                    $('#search-startDate input').val('');
                                }
                            }]
                        });
                    }
                    else if($(".date input:eq(0)").val() != '' && $(".date input:eq(1)").val())
                    {
                            options.table.ajax.reload();
                    }
                }
                
            }
            
        });
        var endDate = $('#search-endDate').datetimepicker({
            language:  'zh-CN',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            showMeridian: 1,
            pickerPosition: "bottom-left"
        }).on("changeDate", function(ev){
            if(ev.date != null)
            {
                endDate=new Date(ev.date.getFullYear(),ev.date.getMonth(),ev.date.getDate(),0,0,0);
                if(startDate!=null&&startDate!='undefined'){
                    if(endDate<startDate){
                        BootstrapDialog.show({
                            title: "错误!",
                            message: "结束日期小于开始日期",
                            type: "type-danger",
                            buttons: [{
                                label: '关闭',
                                cssClass: 'btn-default',
                                action: function(dialog) {
                                    dialog.close();
                                    console.log(ev.date);
                                    ev.date = '';
                                    console.log(ev.date);
                                    $('#search-endDate input').val('');
                                }
                            }]
                        });
                    }
                    else if($(".date input:eq(0)").val() != '' && $(".date input:eq(1)").val())
                    {
                            options.table.ajax.reload();
                    }
                }
                
            }
        });
    }
    $("select.searchNormal").change(function (){
        options.table.ajax.reload();
    });

    $("button.global-search").click(function (){
        options.table.ajax.reload();
    });

}

//cookie类
var cookieTool = {
//读取cookie
    getCookie: function(c_name) {
        if (document.cookie.length > 0) {
                c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
                c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
                return unescape(document.cookie.substring(c_start, c_end));
            }
        }
        return "";
    },
    //设置cookie
    setCookie: function(c_name, value, expiredays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + expiredays); //设置日期
        document.cookie = c_name + "=" + escape(value) + ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString());
    },
    //删除cookie
    delCookie: function(c_name) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() - 1); //昨天日期
        document.cookie = c_name + "=;expires=" + exdate.toGMTString();
    }
};
