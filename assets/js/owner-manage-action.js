$(function (){
    $('button[for="owner-manage-add"]').addDialog('/ajax/owner-manage-add');

    $('button[for="add-owner-info"]').click(function (){
        console.log($(".owner-info").length/2);
        var count = $(".owner-info").length/2;
        count++;
        var tpl = '<div class="clear-10"></div>' +
                  '<div class="form-group owner-info">' +
                    '<label>姓名:</label>' +
                    '<input name="{name}" type="text" class="form-control ml-5">' +
                  '</div>' +
                  '<div class="form-group owner-info">' +
                    '<label>联系方式:</label>' +
                    '<input name="{tel}" type="text" class="form-control ml-5">' +
                  '</div>';
        var name = "name-" + count;
        var tel = "tel-" + count;

        tpl = tpl.replace(/{name}/, name).replace(/{tel}/, tel);
        console.log(tpl);

        $(".owner-info:last").after(tpl);
        console.log(name);
    });

    //  查阅修改
    $(".buttonDetailsActions").buttonDetailsActions('owner-manage', '/ajax/ad');
});