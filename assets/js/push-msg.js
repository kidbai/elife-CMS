$(function (){
    var push_msg = $("#push-msg").DataTable({
        'dom':'i<"right"l>rtp',   // l - length changing input control
                        // f - filtering input
                        // t - The table!
                        // i - Table information summary
                        // p - pagination control
                        // r - processing display element
      //    "processing": true,                 //加载数据时显示正在加载信息
            // "serverSide": true,
        "iDisplaylength":10,
        "sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        },
        // "ajax":{
        //  // "url":"assets/js/scripts/post.php",
        //  // "type":"POST",
        // },
        'order': [1, 'asc'],
    });

    // $.buttonGroupInit('staff-manage', true, true, true, true);
    $(".operation-button").buttonGroupAction({
        tableId: 'push-msg',
        table: push_msg,
        ajaxUrl: '/home/getRepairJson',
        buttonImportExcel: false,
        buttonDelete: false,
        buttonSelectAll: false,
        buttonAdd: true
    })


    //  查阅
    $('button[rel="add-push-msg"]').click(function (){
        layer.open({
            type: 2,
            title: ['新增推送消息', 'font-size:18px;font-family:Lato,sans-serif;text-align:center'],
            shadeClose: true,
            shade: 0.3,
            closeBtn: 2, 
            area: ['500px', '383px'],
            content: 'push_msg/push_msg_add' //iframe的url
        });  
    });

    //  查阅
    $('button[for="push-msg-search"]').click(function (){
        layer.open({
            type: 2,
            title: ['新增推送消息', 'font-size:18px;font-family:Lato,sans-serif;text-align:center'],
            shadeClose: true,
            shade: 0.3,
            closeBtn: 2, 
            area: ['500px', '383px'],
            content: 'push_msg/push_msg_search' //iframe的url
        });  
    });
});

