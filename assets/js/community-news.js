$(function (){
    var community_new = $("#community-new").DataTable({
        'dom':'i<"right"l>rtp',   // l - length changing input control
                        // f - filtering input
                        // t - The table!
                        // i - Table information summary
                        // p - pagination control
                        // r - processing display element
      //    "processing": true,                 //加载数据时显示正在加载信息
            // "serverSide": true,
            "iDisplaylength":10,
            "sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        },
        // "ajax":{
        //  // "url":"assets/js/scripts/post.php",
        //  // "type":"POST",
        // },
        'order': [1, 'asc'],
        'columnDefs': [{
            'targets': 0,
            'searchable':false,
            'orderable':false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                return '<input type="checkbox">';
            }
        }],
        'order': [1, 'asc'],
    });
    

    // 初始化筛选框参数 searchArrayItem
    var searchArray = [];
    var searchArrayItemLabel = {};
    var searchArrayItem = {};
    searchArrayItem['all'] = '全部';
    searchArrayItem['announcement'] = '公告';
    searchArrayItem['news'] = '新闻';
    searchArrayItem['greeting'] = '问候';
    searchArrayItem['other'] = '其他';

    searchArrayItemLabel['信息类型'] = searchArrayItem;
    searchArray.push(searchArrayItemLabel);

    // 全局搜索
    var globalSearch = {};
    globalSearch['roomId'] = '房号';
    globalSearch['userNo'] = '用户编号';
    globalSearch['userPhone'] = '用户手机号码';

    // $.buttonGroupInit('staff-manage', true, true, true, true);
    $(".operation-button").buttonGroupAction({
        tableId: 'community-new',
        table: community_new,
        ajaxUrl: '/home/getRepairJson',
        buttonImportExcel: false,
        buttonDelete: true,
        buttonSelectAll: true,
        buttonAdd: false
    })

    $(".toolbar").toolbarInit({
        tableId: 'community-new',
        table: community_new,
        toolbarSearch: searchArray,
        toolbarGlobalSeach: globalSearch,
        toolbarDateTimePicker: true,
        ajaxUrl: '/home/getRepairJson'
    });
});

