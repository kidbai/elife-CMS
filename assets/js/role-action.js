$(function (){
    $('input[rel="level-1"]').click(function (){
        if($(this).is(":checked"))
        {
            $(this).siblings('.checkbox-item-second').addClass('on');
        }
        else
        {
            $(this).siblings('.checkbox-item-second').removeClass('on');
            $(this).siblings('.checkbox-item-second').children('input').prop("checked", false);
        }
    });

    $('button[for="role-edit"]').click(function (){
        console.log($('form.dialog').serialize());
    });
}); 