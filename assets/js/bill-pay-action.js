$(function (){
    // 未选择select总计
    var initTotal = 0;
    $.each($('td[for="subtotal"]'), function(index, val) {
            initTotal += parseInt($(this).text().substring(1));
        });
    $('td[for="bill-total"]').text('￥' + initTotal);

    $('td[for="count"]').delegate('select', 'click', function (){
        var count = parseInt($(this).val());
        var unitPrice = parseInt($(this).parent().siblings('td[for="unit-price"]').text().substring(1));
        var subtotal = count * unitPrice;
        $(this).parent().siblings('td[for="subtotal"]').text('￥' + subtotal);
        var sumTotal = 0;
        $.each($('td[for="subtotal"]'), function(index, val) {
            sumTotal += parseInt($(this).text().substring(1));
        });
        console.log(sumTotal);
        $('td[for="bill-total"]').text('￥' + sumTotal);
    });

});