function initTable(tableID)
{
    $('#' + tableID).DataTable({
        'dom':'',   // l - length changing input control
                        // f - filtering input
                        // t - The table!
                        // i - Table information summary
                        // p - pagination control
                        // r - processing display element
      //    "processing": true,                 //加载数据时显示正在加载信息
            // "serverSide": true,
        "iDisplaylength":10,
        "sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        },
        // "ajax":{
        //  // "url":"assets/js/scripts/post.php",
        //  // "type":"POST",
        // },
        'columnDefs': [{
            'targets': 0,
            'searchable':false,
            'orderable':false,
        }],
        'order': [],
    });
}
$(function (){
    initTable('table-1');
    initTable('table-2');
    initTable('table-3');
});

