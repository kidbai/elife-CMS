$(function (){
    var staff_manage = $("#staff-manage").DataTable({
        'dom':'i<"right"l>rtp',   // l - length changing input control
                        // f - filtering input
                        // t - The table!
                        // i - Table information summary
                        // p - pagination control
                        // r - processing display element
      //    "processing": true,                 //加载数据时显示正在加载信息
            // "serverSide": true,
        "iDisplaylength":10,
        "sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        },
        // "ajax":{
        //  // "url":"assets/js/scripts/post.php",
        //  // "type":"POST",
        // },
        'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'className': 'dt-body-center',
         'render': function (data, type, full, meta){
             return '<input type="checkbox">';
            }
        }],
        'order': [1, 'asc'],
    });
    

    // 初始化筛选框参数 searchArrayItem
    var searchArray = [];
    var searchArrayItemLabel = {};
    var searchArrayItem = {};
    searchArrayItem['roomId'] = '房间号';
    searchArrayItem['orderNo'] = '保修单';
    searchArrayItem['appUser'] = '报修人';
    searchArrayItem['appUser-phone'] = '报修人电话';

    searchArrayItemLabel['部门'] = searchArrayItem;
    searchArray.push(searchArrayItemLabel);
    searchArray.push(searchArrayItemLabel);

    // 全局搜索
    var globalSearch = {};
    globalSearch['1'] = '电话号码';
    globalSearch['2'] = '地址';

    // $.buttonGroupInit('staff-manage', true, true, true, true);
    $(".operation-button").buttonGroupAction({
        tableId: 'staff-manage',
        table: staff_manage,
        ajaxUrl: '/home/getRepairJson',
        buttonImportExcel: true,
        buttonDelete: true,
        buttonSelectAll: true,
        buttonAdd: true
    })

    // $.toolbarInit(tableId, staff_manage, false, globalSearch, true, ajaxUrl);
    $(".toolbar").toolbarInit({
        tableId: 'staff-manage',
        table: staff_manage,
        toolbarSearch: searchArray,
        toolbarGlobalSeach: globalSearch,
        toolbarDateTimePicker: true,
        ajaxUrl: '/home/getRepairJson'
    })

    //  新增

    $('button[rel="add-staff-manage"]').click(function (){
        layer.open({
            type: 2,
            title: ['新增员工信息', 'font-size:18px;font-family:Lato,sans-serif;text-align:center'],
            shadeClose: true,
            shade: 0.3,
            closeBtn: 2, 
            area: ['500px', '540px'],
            content: 'staff/staff_manage_add' //iframe的url
        });  
    });

    //  查阅
    $('button[for="staff-manage-details"]').click(function (){
        layer.open({
            type: 2,
            title: ['业主信息详情', 'font-size:18px;font-family:Lato,sans-serif;text-align:center'],
            shadeClose: true,
            shade: 0.3,
            closeBtn: 2, 
            area: ['500px', '600px'],
            content: 'staff/staff_manage_details' //iframe的url
        });  
    });
});

