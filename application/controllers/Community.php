<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Community extends MY_Controller {

    public function __construct()
    {
        parent::__construct('web');
    }

    public function index()
    {
        
    }

    public function community_news_add() 
    {
        $this->load->view('layout/header');
        $this->load->view('templates/community-news-edit');
    }

    public function community_news_details()
    {
        $this->load->view('layout/header');
        $this->load->view('templates/community-news-details');
    }

    public function community_news_edit() 
    {
        $this->load->view('layout/header');
        $this->load->view('templates/community-news-edit');
    }

}