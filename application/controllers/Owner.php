<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Owner extends MY_Controller {

    public function __construct()
    {
        parent::__construct('web');
    }

    public function index()
    {
        
    }

    public function owner_manage_add() {
        $this->load->view('layout/header');
        $this->load->view('templates/owner-manage-add');
    }

    public function owner_manage_details() {
        $this->load->view('layout/header');
        $this->load->view('templates/owner-manage-details');
    }

}