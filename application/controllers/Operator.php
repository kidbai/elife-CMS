<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operator extends MY_Controller {

    public function __construct()
    {
        parent::__construct('web');
    }

    public function index()
    {
        
    }

    public function operator_add() {
        $this->load->view('layout/header');
        $this->load->view('templates/operator-edit');
    }

    public function operator_edit() {
        $this->load->view('layout/header');
        $this->load->view('templates/operator-edit');
    }

}