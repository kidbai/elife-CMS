<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role extends MY_Controller {

    public function __construct()
    {
        parent::__construct('web');
    }

    public function index()
    {
        
    }

    public function role_add() {
        $this->load->view('layout/header');
        $this->load->view('templates/role-edit');
    }

    public function role_edit() {
        $this->load->view('layout/header');
        $this->load->view('templates/role-edit');
    }

}