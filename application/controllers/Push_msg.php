<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Push_msg extends MY_Controller {

    public function __construct()
    {
        parent::__construct('web');
    }

    public function index()
    {
        
    }

    public function push_msg_add() {
        $this->load->view('layout/header');
        $this->load->view('templates/push-msg-add');
    }

    public function push_msg_search() {
        $this->load->view('layout/header');
        $this->load->view('templates/push-msg-search');
    }

}