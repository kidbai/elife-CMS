<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feedback extends MY_Controller {

    public function __construct()
    {
        parent::__construct('web');
    }

    public function index()
    {
        
    }

    public function feedback_details() 
    {
        $this->load->view('layout/header');
        $this->load->view('templates/feedback-details');
    }

}