<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        
    }

    public function service_star_details() 
    {
        $this->layout->view('service-star-details');
    }

    public function service_repair_reply_details()
    {
        $this->layout->view('repair-reply-details');
    }

}