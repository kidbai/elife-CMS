<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Guide extends MY_Controller {

    public function __construct()
    {
        parent::__construct('web');
    }

    public function index()
    {
        
    }

    public function guide_add() 
    {
        $this->load->view('layout/header');
        $this->load->view('templates/guide-edit');
    }

    public function guide_edit()
    {
        $this->load->view('layout/header');
        $this->load->view('templates/guide-edit');
    }

    public function guide_details()
    {
        $this->load->view('layout/header');
        $this->load->view('templates/guide-details');
    }

}