<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staff extends MY_Controller {

    public function __construct()
    {
        parent::__construct('web');
    }

    public function index()
    {
        
    }

    public function staff_manage_add() {
        $this->load->view('layout/header');
        $this->load->view('templates/staff-manage-add');
    }

    public function staff_manage_details() {
        $this->load->view('layout/header');
        $this->load->view('templates/staff-manage-details');
    }

}