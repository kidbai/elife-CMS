<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class department extends MY_Controller {

    public function __construct()
    {
        parent::__construct('web');
    }

    public function index()
    {
        
    }

    public function department_manage_add() {
        $this->load->view('layout/header');
        $this->load->view('templates/department-manage-add');
    }

    public function department_manage_details() {
        $this->load->view('layout/header');
        $this->load->view('templates/department-manage-details');
    }

}