<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Repair extends MY_Controller {

    public function __construct()
    {
        parent::__construct('web');
    }

    public function index()
    {
        
    }

    public function repair_add()
    {
        $this->load->view('layout/header');
        $this->load->view('templates/repair-add');
    }

    public function repair_details() 
    {
        $this->load->view('layout/header');
        $this->load->view('templates/repair-details');
    }

}