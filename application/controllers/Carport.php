<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Carport extends MY_Controller {

    public function __construct()
    {
        parent::__construct('web');
    }

    public function index()
    {
        
    }

    public function carport_manage_add() {
        $this->load->view('layout/header');
        $this->load->view('templates/carport-manage-add');
    }

    public function carport_manage_details() {
        $this->load->view('layout/header');
        $this->load->view('templates/carport-manage-details');
    }

}