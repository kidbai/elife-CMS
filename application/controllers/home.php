<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		 // $this->layout->view('index');
        $this->load->view('layout/header');
        $this->load->view('templates/login');
	}

	public function owner_manage()
	{
		$this->layout->view('owner-manage');
	}

	public function staff_manage()
	{
		$this->layout->view('staff-manage');
	}

	public function carport_manage()
	{
		$this->layout->view('carport-manage');
	}

	public function department_manage()
	{
		$this->layout->view('department-manage');
	}

	public function bill_rule()
	{
		$this->layout->view('bill-rule');
	}

	public function resident_bill()
	{
		$this->layout->view('resident-bill');
	}

	public function users_info()
	{
		$this->layout->view('users-info');
	}

	public function repair()
	{
		$this->layout->view('repair');
	}

	public function visitor()
	{
		$this->layout->view('visitor');
	}

	public function public_monitoring()
	{
		$this->layout->view('public-monitoring');
	}

	public function community_news()
	{
		$this->layout->view('community-news');
	}

	public function guide()
	{
		$this->layout->view('guide');
	}

	public function service()
	{
		$this->layout->view('service');
	}

	public function repair_reply()
	{
		$this->layout->view('repair-reply');
	}

	public function feedback()
	{
		$this->layout->view('feedback');
	}

	public function role()
	{
		$this->layout->view('role');
	}

	public function operator()
	{
		$this->layout->view('operator');
	}

	public function operate_log()
	{
		$this->layout->view('operate-log');
	}

	public function statistics()
	{
		$this->layout->view('statistics');
	}

	public function statistics_chart()
	{
		$this->layout->view('statistics-chart');
	}

	public function push_msg()
	{
		$this->layout->view('push-msg');
	}

	public function bill_detail()
	{
		$this->layout->view('bill-detail');
	}

	public function test2(){
		$this->layout->view('test2');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */