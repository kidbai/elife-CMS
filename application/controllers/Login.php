<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {

    public function __construct()
    {
        parent::__construct('web');
    }

    public function index()
    {
        
    }

    public function login() {
        $this->load->view('layout/header');
        $this->load->view('templates/login');
    }

}