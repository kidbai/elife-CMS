<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller {

    public function __construct()
    {
        parent::__construct('web');
    }

    public function index()
    {
        
    }
    public function users_info_details() {
        $this->load->view('layout/header');
        $this->load->view('templates/users-info-details');
    }

}