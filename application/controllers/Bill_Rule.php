<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bill_Rule extends MY_Controller {

    public function __construct()
    {
        parent::__construct('web');
    }

    public function index()
    {
    }

    public function bill_rule_add() {
        $this->load->view('layout/header');
        $this->load->view('templates/bill-rule-add');
    }

    public function bill_rule_details() {
        $this->load->view('layout/header');
        $this->load->view('templates/bill-rule-details');
    }

}