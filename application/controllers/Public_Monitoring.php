<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Public_Monitoring extends MY_Controller {

    public function __construct()
    {
        parent::__construct('web');
    }

    public function index()
    {
        
    }

    public function public_monitoring_details()
    {
        $this->load->view('layout/header');
        $this->load->view('templates/public-monitoring-details');
    }

}