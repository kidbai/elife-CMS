<?php
/*
* 
* --------------------------------------------------------------------
*  控制器超类，加入pjax请求判断
* --------------------------------------------------------------------
*
*  
*/


/* End of file MY_Controller.php */
/* Location: ./application/controllers/MY_Controller.php */
class MY_Controller extends CI_Controller{

	protected $_layout='layout_default';

	public function __construct($type=null){
		parent::__construct();
        if(isset($type)){
        }else{
            $this->_initialize();
        }
	}

	protected function _initialize(){
	    $this->_set_layout();
        // check if is pjax request
        // var_dump(array_key_exists('HTTP_X_PJAX', $_SERVER));
        // var_dump($this->is_pjax());
        if (array_key_exists('HTTP_X_PJAX', $_SERVER) && $_SERVER['HTTP_X_PJAX'])
        {
            $this->layout->disable_layout = TRUE;
        }
        else {
            // echo rand();
        }
	}

	protected function _set_layout(){
        $this->load->library('layout');
        //set template
        $this->layout->set_layout($this->_layout);
	}

    public function setHead()
    {
       echo "hehehhe";
    }
}
/* End of file */