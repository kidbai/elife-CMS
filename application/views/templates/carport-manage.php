<div class="content-header clear">
    <h3 class="heading pull-left">
        <i class="fa fa-wrench"></i>
        车位管理
    </h3>
    <div class="operation-button pull-right"></div>
</div>
<div class="container-fluid">
    <div class="toolbar"></div>
    <table id="carport-manage" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>序号</th>
                <th>车位号</th>
                <th>车辆状态</th>
                <th>车辆所有人编号</th>
                <th>车辆所有人</th>
                <th>车辆号牌</th>
                <th>联系方式</th>
                <th>更新时间</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="table-checkbox">
                    <input type="checkbox">
                </td>
                <td>1</td>
                <td>A101</td>
                <td>闲置</td>
                <td>CD12348917234</td>
                <td>张三</td>
                <td>川A 321ad</td>
                <td>138979874324</td>
                <td>2014-01-01</td>
                <td>
                    <button class="btn btn-info" for="carport-manage-details">查阅</button>
                </td>
            </tr>
            <tr>
                <td class="table-checkbox">
                    <input type="checkbox">
                </td>
                <td>1</td>
                <td>A101</td>
                <td>闲置</td>
                <td>CD12348917234</td>
                <td>张三</td>
                <td>川A 321ad</td>
                <td>138979874324</td>
                <td>2014-01-01</td>
                <td>
                    <button class="btn btn-info" for="carport-manage-details">查阅</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="assets/js/bootstrap-dialog.js"></script>
<script src="assets/js/layer.js"></script>
<script src="assets/js/carport-manage.js"></script>
