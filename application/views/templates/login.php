<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="login-wrapper">
    <div class="login-header">
        <img src="assets/img/icon_hexinyi2.png" alt="">
    </div>
    <div class="login-block">
        <div class="col-md-8 col-sm-8 hidden-xs">
            <div class="login-bg"></div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <form class="login-form">
                <div class="form-group text-center">
                    <div class="login-title">账号密码登陆</div>
                </div>
                <div class="login-divider"></div>
                <div class="form-group col-md-offset-2 col-md-8">
                    <div class="alert alert-danger" role="alert">
                    </div>
                </div>
                <div class="form-group col-md-offset-2 col-md-8">
                    <div class="input-group">
                        <input type="text" name="username" class="form-control">
                        <span class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </span>
                    </div>
                </div>
                <div class="form-group col-md-offset-2 col-md-8">
                    <div class="input-group">
                        <input type="password" name="password" class="form-control">
                        <span class="input-group-addon">
                            <i class="fa fa-key"></i>
                        </span>
                    </div>
                </div>
                <div class="form-group col-md-offset-2 col-md-4">
                    <div class="input-group">
                        <input type="text" name="validatecode" class="form-control" placeholder="验证码">
                    </div>
                </div>
                <div class="form-group col-md-2 col-sm-12 col-xs-8" name="securimage" style="cursor:pointer" >
                   <img id="imgObj" src="propLogin/securimage" width=80 height=36 
                    style="cursor:pointer;" onclick="changeImg()"/>
                </div> 
                <div class="checkbox col-md-offset-2 col-md-10 col-sm-offset-3 col-sm-12 col-xs-12">
                    <label>
                        <input type="checkbox" name="remember-me"> 记住我
                    </label>
                </div>
                <div class="form-group col-md-offset-2 col-md-8">
                    <!-- <input type="submit" class="form-control btn btn-primary" value="登陆">  -->
                    <div class="form-control btn btn-primary" id="login">登陆</div>
                </div>
            </form>
        </div>
    </div>
    
</div>
<div class="clear"></div>
<footer>
    版权所有：成都合心毅科技有限公司 <br> 备案号：<a href="http://www.miitbeian.gov.cn">蜀ICP备15012845号</a>
</footer>

<script>
    function changeImg(){
        var imgSrc = $("#imgObj");
        var src = imgSrc.attr("src");
        imgSrc.attr("src",chgUrl(src));
    }
    //时间戳
    //为了使每次生成图片不一致，即不让浏览器读缓存，所以需要加上时间戳
    function chgUrl(url){
        var timestamp = (new Date()).valueOf();
        url = url.substring(0,17);
        if((url.indexOf("&")>=0)){
            url = url + "×tamp=" + timestamp;
        }else{
            url = url + "?timestamp=" + timestamp;
        }
        return url;
    }
    function isRightCode(){
        var code = $("#veryCode").attr("value");
        code = "c=" + code;
        $.ajax({
            type:"POST",
            url:"propLogin/securimage",
            data:code,
            success:callback
        });
    }
    function callback(data){
        //$("#info").html(data);
    }
</script>
<script src="assets/js/jquery-1.11.2.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/admin-login.js"></script>