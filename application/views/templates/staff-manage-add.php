<form class="form-inline dialog">

  <div class="form-group inline-block fl" style="width:50%">
    <label>姓名:</label>
    <input name="staffName" type="text" class="form-control" value="" >
    <div class="clear-5"></div>
    <label>性别:</label>
    <input type="radio" name="staffGender" value="male" checked > 男
    <input type="radio" name="staffGender" value="female" > 女
    <div class="clear-5"></div>
    <label>年龄:</label>
    <input name="staffAge" type="text" class="form-control" value="" >
    <div class="clear-5"></div>
    <label>联系方式:</label>
    <input name="tel" type="text" class="form-control" value="" >
    <div class="clear-5"></div>
    <label>身份证号:</label>
    <input name="idNo" type="text" class="form-control" value="" >
  </div>
  <!-- *****上传照片***** -->
  <div class="form-group inline-block mb-0 fl text-center" style="width:50%">
    <!-- The fileinput-button span is used to style the file input field as button -->
    <div class="clear"></div>
    <span class="btn fileinput-button" style="padding:0">
        <img id="staff-avatar" src="" alt="" width="150" height="200">
        <input id="fileupload" style="width:150px; height:200px" type="file" name="files[]" multiple>
        <!-- The global progress bar -->
        <div id="progress" class="progress display-none">
            <div class="progress-bar progress-bar-success"></div>
        </div>
    </span>
    <input class="display-none" type="text" name="file-name" value="">
    
    <!-- The container for the uploaded files -->
    <div id="files" class="files"></div>
  </div> 
  <div class="clear-10-divider"></div>
  <div class="form-group inline-block fl" style="width:50%">
    <label>部门:</label>
    <select name="department" class="form-control">
      <option value="1">安保部</option>
      <option value="1">安保部</option>
      <option value="1">安保部</option>
      <option value="1">安保部</option>
    </select>
    <div class="clear-5"></div>
    <label>职位:</label>
    <select name="department" class="form-control">
      <option value="2">保安</option>
      <option value="2">保安</option>
      <option value="2">保安</option>
      <option value="2">保安</option>
    </select>
    <div class="clear-5"></div>
    <label>入职日期:</label>
    <div class="input-group date" id="entryDate" data-date-format="yyyy-MM-dd">
        <input class="form-control" name="entryDate" type="text" rel="startDate" readonly>
        <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
        <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
    </div>
    <div class="clear-5"></div>
    <label>备注:</label>
    <textarea name="remark" class="form-control" id=""></textarea>
  </div>
  <div class="clear-10"></div>
  <div class="form-group text-center">
    <button type="button" class="btn btn-success" for="staff-manage-add">提交</button>
  </div>
</form>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="assets/js/jquery.ui.widget.js"></script>
<script src="assets/js/jquery.fileupload.js"></script>
<script src="assets/js/jquery.fileupload-process.js"></script>
<script src="assets/js/layer.js"></script>
<script src="assets/js/staff-manage-action.js"></script>
