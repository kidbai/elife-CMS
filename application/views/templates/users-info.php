<div class="content-header clear">
    <h3 class="heading pull-left">
        <i class="fa fa-wrench"></i>
        用户信息
    </h3>
</div>
<div class="container-fluid">
    <div class="toolbar"></div>
    <table id="users-info" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>用户编号</th>
                <th>姓名</th>
                <th>性别</th>
                <th>联系方式</th>
                <th>房号</th>
                <th>角色</th>
                <th>车位情况</th>
                <th>用户状态</th>
                <th>注册时间</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>CD3897893123</td>
                <td>张三</td>
                <td>男</td>
                <td>12387123124</td>
                <td>1-1-1</td>
                <td>租户</td>
                <td>有</td>
                <td>激活</td>
                <td>2015-07-22 10：14</td>
                <td>
                    <button class="btn btn-info" for="users-info-details">查阅</button>
                </td>
            </tr>
            <tr>
                <td>CD3897893123</td>
                <td>张三</td>
                <td>女</td>
                <td>12387123124 | 163192837</td>
                <td>1-1-1</td>
                <td>业主</td>
                <td>无</td>
                <td>未激活</td>
                <td>---</td>
                <td>
                    <button class="btn btn-info" for="users-info-details">查阅</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="assets/js/bootstrap-dialog.js"></script>
<script src="assets/js/layer.js"></script>
<script src="assets/js/users-info.js"></script>
