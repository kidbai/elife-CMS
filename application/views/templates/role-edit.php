<form class="form-inline dialog">
  <div class="form-group">
    <label>角色名称:</label>
    <input name="roleName" type="text" class="form-control ml-5" value="">
  </div>
  <div class="form-group">
    <label>角色备注:</label>
    <input name="roleRemark" type="text" class="form-control ml-5" value="">
  </div>
  <div class="clear-10"></div>
  <div class="form-group">
    <label>角色权限:</label>
    <div class="checkbox-wrapper">
      <div class="checkbox-block">
        <div class="checkbox-item">
          <label>-物业缴费</label>
          <input type="checkbox" name="role" rel="level-1" value="1">
          <div class="checkbox-item-second">
            <label>住户账单</label>
            <input type="checkbox" name="role" value="1-1">
            <div class="clear"></div>
            <label>缴费详情</label>
            <input type="checkbox" name="role" value="1-2">
          </div>
        </div>
        <div class="checkbox-item">
          <label>-社区服务</label>
          <input type="checkbox" name="role" rel="level-1" value="2">
          <div class="checkbox-item-second">
            <label>用户信息</label>
            <input type="checkbox" name="role" value="2-1">
            <div class="clear"></div>
            <label>家用报修</label>
            <input type="checkbox" name="role" value="2-2">
            <div class="clear"></div>
            <label>访客通行</label>
            <input type="checkbox" name="role" value="2-3">
            <div class="clear"></div>
            <label>公共监察</label>
            <input type="checkbox" name="role"value="2-4">
            <div class="clear"></div>
            <label>反馈建议</label>
            <input type="checkbox" name="role" value="2-5">
          </div>
        </div>
        <div class="checkbox-item">
          <label>-便民信息</label>
          <input type="checkbox" name="role" rel="level-1" value="3">
          <div class="checkbox-item-second">
            <label>小区动态</label>
            <input type="checkbox" name="role" value="3-1">
            <div class="clear"></div>
            <label>办事指南</label>
            <input type="checkbox" name="role" value="3-2">
          </div>
        </div>
      </div>
      <div class="checkbox-block">
        <div class="checkbox-item">
          <label>-小区管理</label>
          <input type="checkbox" name="role" rel="level-1" value="4">
          <div class="checkbox-item-second">
            <label>统计分析</label>
            <input type="checkbox" name="role" value="4-1">
            <div class="clear"></div>
            <label>服务监督</label>
            <input type="checkbox" name="role" value="4-2">
            <div class="clear"></div>
            <label>账单规则</label>
            <input type="checkbox" name="role" value="4-3">
            <div class="clear"></div>
            <label>业主管理</label>
            <input type="checkbox" name="role" value="4-4">
            <div class="clear"></div>
            <label>车位管理</label>
            <input type="checkbox" name="role" value="4-5">
            <div class="clear"></div>
            <label>员工管理</label>
            <input type="checkbox" name="role" value="4-6">
            <div class="clear"></div>
            <label>部门管理</label>
            <input type="checkbox" name="role" value="4-7">
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="clear-10"></div>
  <div class="form-group text-center">
      <button type="button" class="btn btn-primary" for="role-edit">确认</button>
  </div>
</form>
<script src="assets/js/layer.js"></script>
<script src="assets/js/role-action.js"></script>
