<div class="content-header clear">
    <h3 class="heading pull-left">
        <i class="fa fa-wrench"></i>
        账单管理
    </h3>
    <div class="operation-button pull-right"></div>
</div>
<div class="container-fluid">
    <div class="toolbar"></div>
    <table id="bill-rule" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>序号</th>
                <th>账单类型</th>
                <th>账单生成周期</th>
                <th>收费标准</th>
                <th>账单备注</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="table-checkbox">
                    <input type="checkbox">
                </td>
                <td>1</td>
                <td>物业费</td>
                <td>每月01日</td>
                <td>2.6元/m2</td>
                <td>备注备注备注</td>
                <td>
                    <button class="btn btn-info" for="bill-rule-details">查阅</button>
                </td>
            </tr>
            <tr>
                <td class="table-checkbox">
                    <input type="checkbox">
                </td>
                <td>1</td>
                <td>机动车停车费</td>
                <td>每月01日</td>
                <td>2.6元/m2</td>
                <td>备注备注备注</td>
                <td>
                    <button class="btn btn-info" for="bill-rule-details">查阅</button>
                </td>
            </tr>
            <tr>
                <td class="table-checkbox">
                    <input type="checkbox">
                </td>
                <td>1</td>
                <td>清洁费</td>
                <td>每月01日</td>
                <td>2.6元/m2</td>
                <td>备注备注备注</td>
                <td>
                    <button class="btn btn-info" for="bill-rule-details">查阅</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<script src="assets/js/bootstrap-dialog.js"></script>
<script src="assets/js/layer.js"></script>
<script src="assets/js/bill-rule.js"></script>
