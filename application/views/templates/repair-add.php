<form class="form-inline label-100 dialog">
  <div class="form-group owner-info">
    <label>房号:</label>
    <input name="roomId" type="text" class="form-control">
  </div>
  <div class="clear-10"></div>
  <div class="form-group owner-info">
    <label>报修人:</label>
    <input name="appUser" type="text" class="form-control">
  </div>
  <div class="clear-10"></div>
  <div class="form-group">
    <label>报修人电话:</label>
    <input name="buildArea" type="text" class="form-control">
  </div>
  <div class="clear-10"></div>
  <div class="form-group">
    <label>预约时间:</label>
    <div class="input-group date" id="appointmentTime" data-date-format="yyyy-MM-dd - HH:ii P">
        <input class="form-control" name="appointmentTime" type="text" rel="startDate" readonly>
        <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
        <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
    </div>
  </div>
  <div class="clear-10"></div>
  <div class="form-group">
    <label>报修描述:</label>
    <input name="roomArea" type="text" class="form-control">
  </div>
  <div class="clear-10"></div>
  <div class="form-group">
    <label>备注:</label>
    <textarea name="remark" class="form-control" id=""></textarea>
  </div>
  <div class="clear-10"></div>
  <!-- *****上传照片***** -->
  <div class="form-group">
    <!-- The fileinput-button span is used to style the file input field as button -->
    <label>添加图片:</label>
    <span class="btn fileinput-button" style="padding:0">
        <img id="staff-avatar" src="" alt="" width="100" height="120">
        <input id="fileupload" style="width:100px; height:120px" type="file" name="files[]" multiple>
        <!-- The global progress bar -->
        <div id="progress" class="progress display-none">
            <div class="progress-bar progress-bar-success"></div>
        </div>
        <div id="files" class="files"></div>
    </span>
    <input class="display-none" type="text" name="file-name" value="">
    
    <!-- The container for the uploaded files -->
  </div> 
  <div class="clear-10"></div>
  <div class="form-group text-center">
    <button type="button" class="btn btn-success" for="repair-add">提交</button>
  </div>
</form>

<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="assets/js/jquery.ui.widget.js"></script>
<script src="assets/js/jquery.fileupload.js"></script>
<script src="assets/js/jquery.fileupload-process.js"></script>
<script src="assets/js/layer.js"></script>
<script src="assets/js/repair-action.js"></script>

