<div class="content-header clear">
    <h3 class="heading pull-left">
        <i class="fa fa-wrench"></i>
        家用报修
    </h3>
    <div class="operation-button pull-right"></div>
</div>
<div class="container-fluid">
    <div class="toolbar"></div>
    <table id="repair" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>保修单</th>
                <th>房号</th>
                <th>预约时间</th>
                <th>用户编号</th>
                <th>姓名</th>
                <th>联系方式</th>
                <th>报修时间</th>
                <th>报修状态</th>
                <th>维修人信息</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>131241241324</td>
                <td>1-1-101</td>
                <td>-7-15 上午 09:00-12:00</td>
                <td>cd123134123</td>
                <td>张三</td>
                <td>134857494883</td>
                <td>2015-07-12 12:18:14</td>
                <td>待处理</td>
                <td>---</td>
                <td>
                    <button class="btn btn-info" for="repair-details">查阅</button>
                </td>
            </tr>
            <tr>
                <td>131241241324</td>
                <td>1-1-101</td>
                <td>-7-15 上午 09:00-12:00</td>
                <td>cd123134123</td>
                <td>张三</td>
                <td>134857494883</td>
                <td>2015-07-12 12:18:14</td>
                <td>处理中</td>
                <td>312312|小张|137814798</td>
                <td>
                    <button class="btn btn-info" for="repair-details">查阅</button>
                </td>
            </tr>
            <tr>
                <td>131241241324</td>
                <td>1-1-101</td>
                <td>-7-15 上午 09:00-12:00</td>
                <td>cd123134123</td>
                <td>张三</td>
                <td>134857494883</td>
                <td>2015-07-12 12:18:14</td>
                <td>已完成</td>
                <td>312312|小张|137814798</td>
                <td>
                    <button class="btn btn-info" for="repair-details">查阅</button>
                </td>
            </tr>
            <tr>
                <td>131241241324</td>
                <td>1-1-101</td>
                <td>-7-15 上午 09:00-12:00</td>
                <td>cd123134123</td>
                <td>张三</td>
                <td>134857494883</td>
                <td>2015-07-12 12:18:14</td>
                <td>无效</td>
                <td>---</td>
                <td>
                    <button class="btn btn-info" for="repair-details">查阅</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="assets/js/bootstrap-dialog.js"></script>
<script src="assets/js/layer.js"></script>
<script src="assets/js/repair.js"></script>
