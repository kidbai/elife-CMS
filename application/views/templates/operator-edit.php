<form class="form-inline label-90 dialog" style="padding:30px 70px 30px 70px;">
    <div class="form-group inline-block">
        <label>角色:</label>
        <select name="role" class="form-control">
          <option value="1">客服值班员</option>
          <option value="2">客服值班员2</option>
        </select>
        <div class="clear-5"></div>
        <label>账号:</label>
        <input name="ID" type="text" class="form-control" value="">
        <div class="clear-5"></div>
        <label>初始密码:</label>
        <input type="text" name="initPwd" value="" class="form-control">
    </div>
    <div class="clear-10"></div>
    <div class="form-group inline-block" >
        <label>工号:</label>
        <input type="text" name="jobNo" class="form-control" value="">
        <button class="btn btn-primary" type="button" style="padding:0; height=:24px; font-size:13px;">确定</button>
        <div class="clear-5"></div>
        <label>姓名</label>
        <input type="text" class="form-control" readonly>
        <div class="clear-5"></div>
        <label>部门</label>
        <input type="text" class="form-control" readonly>
        <div class="clear-5"></div>
        <label>性别</label>
        <input type="text" class="form-control" readonly>
        <div class="clear-5"></div>
        <label>联系方式</label>
        <input type="text" class="form-control" readonly>

    </div>
    <div class="form-group inline-block width-100 text-center">
        <button class="btn btn-info" for="operator-submit">确认</button>
    </div>
</form>
<script src="assets/js/layer.js"></script>
<script src="assets/js/operator-action.js"></script>
