<form class="form-inline label-90 dialog">
  <div class="form-group">
    <label class="contron-label">房号:</label>
    <select name="buildingFirstNo" class="form-control ml-5 mr-5 width-50" disabled>
      <option value="1">1</option>
    </select>
    <div class="inline-block">
      栋
    </div>
    <select name="buildingSecNo" class="form-control ml-5 mr-5 width-50" disabled>
      <option value="1">1</option>
    </select>
    <div class="inline-block">
      单元
    </div>
    <select name="buildingThrNo" class="form-control ml-5 mr-5 width-50" disabled>
      <option value="1">1</option>
    </select>
    <div class="inline-block">
      号
    </div>
  </div>
  <div class="clear-10"></div>
  <div class="form-group owner-info">
    <label>姓名:</label>
    <input name="name-1" type="text" class="form-control ml-5" value="张三" disabled>
  </div>
  <div class="form-group owner-info">
    <label>联系方式:</label>
    <input name="tel-1" type="text" class="form-control ml-5" value="1888888888" disabled>
  </div>
  <div class="clear-10"></div>
  <div class="form-group">
    <label>建筑面积: (平方米)</label>
    <input name="buildArea" type="text" class="form-control ml-5" value="120" disabled>
  </div>
  <div class="form-group">
    <label>室内面积: (平方米)</label>
    <input name="roomArea" type="text" class="form-control ml-5" value="120" disabled>
  </div>
  <div class="clear-10"></div>
  <div class="form-group">
    <label>备注:</label>
    <input name="remark" type="text" class="form-control ml-5" value="无" disabled>
  </div>
  <div class="clear-10"></div>
  <div class="form-group text-center buttonDetailsActions">
  </div>
</form>
<script src="assets/js/layer.js"></script>
<script src="assets/js/owner-manage-action.js"></script>
