<form class="form-inline label-90 dialog">
  <div class="form-group">
    <label>部门名称:</label>
    <input name="departmentName" type="text" value="安保部" class="form-control" disabled>
  </div>
  <div class="form-group">
    <label>部门备注:</label>
    <input name="departmentRemark" type="text" value="负责小区内的安保事宜" class="form-control" disabled>
  </div>
  <div class="clear"></div>
  <div class="form-group department-position">
    <label>部门职位:</label>
    <div class="label label-info mr-5">
      <div class="label-content inline-block" contenteditable="true">队长</div>
      <a><i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i></a> 
    </div>
    <div class="label label-info mr-5">
      <div class="label-content inline-block" contenteditable="true">副队长</div>
      <a><i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i></a> 
    </div>
    <div class="label label-info mr-5">
      <div class="label-content inline-block" contenteditable="true">普通保安</div>
      <a><i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i></a> 
    </div>
    <div class="tag label label-success label-plus mr-5 ">
      <div class="inline-block">新增</div>
      <a><i class="glyphicon glyphicon-plus-sign glyphicon-white"></i></a> 
    </div>
    <!-- <button type class="btn btn-success btn-plus">新增<i class="glyphicon glyphicon-plus-sign glyphicon-white"></i></button> -->
  </div>
  <div class="form-group">
    <label>负责人:</label>
    <input name="personInCharge" type="text" class="form-control" disabled>
  </div>
  <div class="clear"></div>
  <div class="form-group">
    <label>联系方式:</label>
    <input name="tel" type="text" class="form-control" disabled>
  </div>
  <div class="clear-10"></div>
  <div class="form-group text-center">
    <button class="btn btn-success" for="department-submit">确认</button>
  </div>
</form>
<script src="assets/js/layer.js"></script>
<script src="assets/js/department-manage-action.js"></script>
