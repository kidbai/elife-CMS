<div class="content-header clear">
    <h3 class="heading pull-left">
        <i class="fa fa-wrench"></i>
        服务监督
    </h3>
    <div class="operation-button pull-right">
        <a href="home/service" class="pjaxTrigger btn btn-danger">服务之星</a>
        <a href="home/repair_reply" class="pjaxTrigger btn btn-default">报修评价</a>
    </div>
</div>
<div class="container-fluid">
    <div class="toolbar"></div>
    <table id="service" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>序号</th>
                <th>工号</th>
                <th>姓名</th>
                <th>性别</th>
                <th>部门</th>
                <th>本月赞数</th>
                <th>评论数</th>
                <th>更新时间</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td></td>
                <td>1</td>
                <td>CD123134123</td>
                <td>张三</td>
                <td>安保部</td>
                <td>8</td>
                <td>85</td>
                <td>5</td>
                <td>2015-07-07 11:58</td>
                <td>
                    <a href="service/service_star_details" class="pjaxTrigger btn btn-info">查询</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>1</td>
                <td>CD123134123</td>
                <td>张三</td>
                <td>安保部</td>
                <td>8</td>
                <td>85</td>
                <td>5</td>
                <td>2015-07-07 11:58</td>
                <td>
                    <a href="service/service_star_details" class="pjaxTrigger btn btn-info">查询</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>1</td>
                <td>CD123134123</td>
                <td>张三</td>
                <td>安保部</td>
                <td>8</td>
                <td>85</td>
                <td>5</td>
                <td>2015-07-07 11:58</td>
                <td>
                    <a href="service/service_star_details" class="pjaxTrigger btn btn-info">查询</a>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<script src="assets/js/layer.js"></script>
<script src="assets/js/service.js"></script>
