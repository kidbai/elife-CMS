<form class="form-inline label-90 dialog">
  <div class="form-group">
    <label>部门名称:</label>
    <input name="deparmentName" type="text" class="form-control">
  </div>
  <div class="form-group">
    <label>部门备注:</label>
    <input name="departmentRemark" type="text" class="form-control">
  </div>
  <div class="clear"></div>
  <div class="form-group department-position">
    <label>部门职位:</label>
    <input name="departmentPosition-1" type="text" class="form-control">
  </div>
  <div class="form-group">
    <label>负责人:</label>
    <input name="OwnerName" type="text" class="form-control">
  </div>
  <div class="clear"></div>
  <div class="form-group">
    <label>联系方式:</label>
    <input name="tel" type="text" class="form-control">
  </div>
  <div class="clear-10"></div>
  <div class="form-group text-center">
    <button type="button" class="btn btn-success" for="department-position-add">新增部门职位</button>
    <button type="button" class="btn btn-success" for="department-manage-add">提交</button>
  </div>
</form>
<script src="assets/js/layer.js"></script>
<script src="assets/js/department-manage-action.js"></script>
