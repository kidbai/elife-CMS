<div class="wrapper-edit">
    <div class="container-900">
        <div class="page">
            <form>
                <header class="community-header">
                    <textarea name="doc_title" placeholder="文档标题" ></textarea>
                </header>

                <textarea name="doc_content" id="doc_content"></textarea>
                <div class="form-group">
                    <div class="community-item ml-20">
                        <label>顶置图片:</label>
                        <div class="clear"></div>
                        <span class="btn fileinput-button" style="padding:0; border:1px solid #ccc; border-radius: 4px!important; background-color:#fff;">
                            <img id="staff-avatar" src="" alt="" width="120" height="100">
                            <input id="fileupload" style="width:120px; height:100px" type="file" name="files[]" multiple>
                            <div id="progress" class="progress display-none">
                                <div class="progress-bar progress-bar-success"></div>
                            </div>
                            <div id="files" class="files"></div>
                        </span>
                        <input class="display-none" type="text" name="community-news-top-pic" value="">
                    </div>
                    <div class="community-item ml-20">
                        <label>信息类型:</label>
                        <select class="form-control community-info-type" name="community-type">
                            <option value="announcement">公告</option>
                            <option value="news">新闻</option>
                            <option value="greeting">问候</option>
                            <option value="other">其他</option>
                        </select>
                    </div>
                </div>
                
                
                <div class="clear-10"></div>
                <div class="form-group text-center">
                    <button type="button" class="btn btn-default ml-10" for="community-news-cancel">取消</button>
                    <button type="button" class="btn btn-primary ml-10" for="community-news-submit">发布</button>
                    <!-- <button type="button" class="btn btn-info ml-10" for="community-news-preview">预览</button> -->
                </div>    
            </form>
        </div>
    </div>
    
</div>


<script src="assets/js/jquery.ui.widget.js"></script>
<script src="assets/js/jquery.fileupload.js"></script>
<script src="assets/js/jquery.fileupload-process.js"></script>
<script src="assets/js/jquery.hotkeys.js"></script>
<script src="assets/js/kindeditor-all-min.js"></script>
<script src="assets/js/zh-CN.js"></script>
<script src="assets/js/community-news-action.js"></script>
