<div class="content-header clear">
    <h3 class="heading pull-left">
        <i class="fa fa-wrench"></i>
        办事指南
    </h3>
    <div class="operation-button pull-right">
        <a href="guide/guide_add" target="_blank" class="btn btn-success">新增</a>
    </div>
</div>
<div class="container-fluid">
    <div class="toolbar"></div>
    <table id="guide" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>序号</th>
                <th>标题</th>
                <th>更新时间</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td></td>
                <td>1</td>
                <td>结婚证办理资料和流程</td>
                <td>2015-07-22 09：58</td>
                <td>
                    <a href="guide/guide_details" target="_blank" class="btn btn-info">查询</a>
                    <a href="guide/guide_edit" target="_blank" class="btn btn-success">编辑</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>2</td>
                <td>居住证办理资料和流程</td>
                <td>2015-07-22 09：58</td>
                <td>
                    <a href="guide/guide_details" target="_blank" class="btn btn-info">查询</a>
                    <a href="guide/guide_edit" target="_blank" class="btn btn-success">编辑</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>3</td>
                <td>护照办理资料和流程</td>
                <td>2015-07-22 09：58</td>
                <td>
                    <a href="guide/guide_details" target="_blank" class="btn btn-info">查询</a>
                    <a href="guide/guide_edit" target="_blank" class="btn btn-success">编辑</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>4</td>
                <td>关于7月22日的停水通知</td>
                <td>2015-07-22 09：58</td>
                <td>
                    <a href="guide/guide_details" target="_blank" class="btn btn-info">查询</a>
                    <a href="guide/guide_edit" target="_blank" class="btn btn-success">编辑</a>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="assets/js/bootstrap-dialog.js"></script>
<script src="assets/js/layer.js"></script>
<script src="assets/js/guide.js"></script>
