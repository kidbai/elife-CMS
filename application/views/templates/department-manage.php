<div class="content-header clear">
    <h3 class="heading pull-left">
        <i class="fa fa-wrench"></i>
        部门管理
    </h3>
    <div class="operation-button pull-right"></div>
</div>
<div class="container-fluid">
    <div class="toolbar"></div>
    <table id="department-manage" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>序号</th>
                <th>部门名称</th>
                <th>部门备注</th>
                <th>部门职位</th>
                <th>部门负责人</th>
                <th>负责人联系方式</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="table-checkbox">
                    <input type="checkbox">
                </td>
                <td>1</td>
                <td>安保部</td>
                <td>负责小区内的安保事宜，3小时巡逻一次</td>
                <td>队长|副队长|不同保安</td>
                <td>张三</td>
                <td>123987198274</td>
                <td>
                    <button class="btn btn-info" for="department-manage-details">查阅</button>
                </td>
            </tr>
            <tr>
                <td class="table-checkbox">
                    <input type="checkbox">
                </td>
                <td>1</td>
                <td>安保部</td>
                <td>负责小区内的安保事宜，3小时巡逻一次</td>
                <td>队长|副队长|不同保安</td>
                <td>张三</td>
                <td>123987198274</td>
                <td>
                    <button class="btn btn-info" for="department-manage-details">查阅</button>
                </td>
            </tr>
            <tr>
                <td class="table-checkbox">
                    <input type="checkbox">
                </td>
                <td>1</td>
                <td>安保部</td>
                <td>负责小区内的安保事宜，3小时巡逻一次</td>
                <td>队长|副队长|不同保安</td>
                <td>张三</td>
                <td>123987198274</td>
                <td>
                    <button class="btn btn-info" for="department-manage-details">查阅</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="assets/js/bootstrap-dialog.js"></script>
<script src="assets/js/layer.js"></script>
<script src="assets/js/department-manage.js"></script>
