<div class="content-header clear">
    <h3 class="heading pull-left">
        <i class="fa fa-wrench"></i>
        住户账单
    </h3>
    <div class="operation-button pull-right"></div>
</div>
<div class="container-fluid">
    <div class="toolbar"></div>
    <div class="clear"></div>
    <div class="form-inline mb-20 ml-10">
        <div class="form-group ml-10">
            <label>房号:</label>
            <input type="text" class="form-control" value="1-1-101" readonly>
        </div>
        <div class="form-group ml-10">
            <label>建筑面积:</label>
            <input type="text" class="form-control" value="102.5m2" readonly>
        </div>
        <div class="form-group ml-10">
            <label>车位号:</label>
            <input type="text" class="form-control" value="A 101" readonly>
        </div>
        <div class="clear-10"></div>
        <div class="form-group ml-10">
            <label>业主:</label>
            <input type="text" class="form-control" value="李华成 | 张子佳" readonly>
        </div>
        <div class="form-group ml-10">
            <label>联系方式:</label>
            <input type="text" class="form-control" value="159812124 | 1241827647812" readonly>
        </div>
        <div class="form-group ml-10">
            <label>车牌号:</label>
            <input type="text" class="form-control" value="川A 12345" readonly>
        </div>
        <div class="clear-10"></div>
        <div class="form-group right mb-10">
            <button class="btn btn-danger bill-charge" disabled>收费</button>
        </div>
    </div>
    <table id="resident-bill" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>月份|费用</th>
            </tr>
        </thead>
    </table>
</div>
<script src="assets/js/layer.js"></script>
<script src="assets/js/resident-bill.js"></script>
