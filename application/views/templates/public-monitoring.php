<div class="content-header clear">
    <h3 class="heading pull-left">
        <i class="fa fa-wrench"></i>
        公共监察
    </h3>
    <div class="operation-button pull-right"></div>
</div>
<div class="container-fluid">
    <div class="toolbar"></div>
    <table id="public-monitoring" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>序号</th>
                <th>用户编号</th>
                <th>房号</th>
                <th>用户</th>
                <th>用户电话</th>
                <th>监察类型</th>
                <th>提交时间</th>
                <th>回复条数</th>
                <th>状态</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>1</th>
                <td>CD123134123</td>
                <td>1-1-101</td>
                <td>张三</td>
                <td>1379817249</td>
                <td>设备</td>
                <td>2015-07-12 12：18：56</td>
                <td>5</td>
                <td>待处理</td>
                <td>
                    <button class="btn btn-info" for="public-monitoring-details">查阅</button>
                </td>
            </tr>
            <tr>
                <th>1</th>
                <td>CD123134123</td>
                <td>1-1-101</td>
                <td>张三</td>
                <td>1379817249</td>
                <td>人员</td>
                <td>2015-07-12 12：18：56</td>
                <td>5</td>
                <td>处理中</td>
                <td>
                    <button class="btn btn-info" for="public-monitoring-details">查阅</button>
                </td>
            </tr>
            <tr>
                <th>1</th>
                <td>CD123134123</td>
                <td>1-1-101</td>
                <td>张三</td>
                <td>1379817249</td>
                <td>环境</td>
                <td>2015-07-12 12：18：56</td>
                <td>5</td>
                <td>已完成</td>
                <td>
                    <button class="btn btn-info" for="public-monitoring-details">查阅</button>
                </td>
            </tr>
            <tr>
                <th>1</th>
                <td>CD123134123</td>
                <td>1-1-101</td>
                <td>张三</td>
                <td>1379817249</td>
                <td>其他</td>
                <td>2015-07-12 12：18：56</td>
                <td>5</td>
                <td>无效</td>
                <td>
                    <button class="btn btn-info" for="public-monitoring-details">查阅</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="assets/js/bootstrap-dialog.js"></script>
<script src="assets/js/layer.js"></script>
<script src="assets/js/public-monitoring.js"></script>
