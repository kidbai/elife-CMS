<link rel="stylesheet" href="assets/css/blueimp-gallery/blueimp-gallery.min.css">
<header class="service-header form-group">
    报修评价详情 
</header>
<div class="service-info">
    <div class="form-group service-info-block">
        <label>工号:</label>
        <div class="inline-block">CD378987899</div>
        <div class="clear"></div>
        <label>姓名:</label>
        <div class="inline-block">张三</div>
        <div class="clear"></div>
        <label>性别:</label>
        <div class="inline-block">男</div>
        <div class="clear"></div>
        <label>年龄:</label>
        <div class="inline-block">36</div>
    </div>
    <div class="form-group service-info-block">
        <label>部门:</label>
        <div class="inline-block">保安部</div>
        <div class="clear"></div>
        <label>职位:</label>
        <div class="inline-block">普通保安</div>
        <div class="clear"></div>
        <label>联系方式:</label>
        <div class="inline-block">13812489124</div>
    </div>
    <div class="form-group service-info-block">
        <label>服务评分:</label>
        <div class="inline-block">3(一般)</div>
        <div class="clear"></div>
        <label>技能评论:</label>
        <div class="inline-block">4(一般)</div>
    </div>
    <div class="form-group service-info-block">
        <img src="assets/img/avatar2.jpg" alt="">
    </div>
</div>
<div class="service-repair-details">
    <div class="form-group service-info-block">
        <label>房号:</label>
        <div class="inline-block">1-1-101</div>
        <div class="clear"></div>
        <label>报修人:</label>
        <div class="inline-block">李三</div>
        <div class="clear"></div>
        <label>报修人电话:</label>
        <div class="inline-block">138217498134</div>
    </div>
    <div class="form-group service-info-block">
        <label>报修单号:</label>
        <div class="inline-block">143124718924</div>
        <div class="clear"></div>
        <label>预约时间:</label>
        <div class="inline-block">07-15 上午</div>
        <div class="clear"></div>
        <label>提交时间:</label>
        <div class="inline-block">2015-07-12 12:18:57</div>
    </div>
    <div class="clear"></div>
    <div class="form-group" style="padding-left:4em">
        <label>报修描述:</label>
        <div class="inline-block">我家厨房水管有漏水现象，麻烦物业来</div>
        <div id="repair-img">
            <a href="assets/img/avatar2.jpg" title='报修图片1'>
                <img src="assets/img/avatar2.jpg" for="public-monitoring-img" width="100" height="100">
            </a>
            <a href="assets/img/avatar.png" title='报修图片2'>
                <img src="assets/img/avatar.png" for="public-monitoring-img" width="100" height="100">
            </a>
            <a href="assets/img/avatar2.jpg" title='报修图片3'>
                <img src="assets/img/avatar2.jpg" for="public-monitoring-img" width="100" height="100">
            </a>
        </div> 
    </div>
    <div class="form-group" style="padding-left:4em">
        <label>报修评价:</label>
        <div class="inline-block">师傅做的不错</div>
        <div id="repair-feedback-img">
            <a href="assets/img/avatar2.jpg" title='报修图片1'>
                <img src="assets/img/avatar2.jpg" for="public-monitoring-img" width="100" height="100">
            </a>
            <a href="assets/img/avatar.png" title='报修图片2'>
                <img src="assets/img/avatar.png" for="public-monitoring-img" width="100" height="100">
            </a>
            <a href="assets/img/avatar2.jpg" title='报修图片3'>
                <img src="assets/img/avatar2.jpg" for="public-monitoring-img" width="100" height="100">
            </a>
        </div> 
    </div>
</div>
<div id="blueimp-gallery" class="blueimp-gallery">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
</div>
<script src="assets/js/blueimp-gallery.min.js"></script>
<script src="assets/js/service-repair-reply-details.js"></script>
