<form class="form-inline dialog">

  <div class="form-group inline-block fl" style="width:50%">
    <label>工号:</label>
    <input name="staffNo" type="text" class="form-control" value="CD912837123" disabled>
    <div class="clear-5"></div>
    <label>姓名:</label>
    <input name="staffName" type="text" class="form-control" value="张三" disabled>
    <div class="clear-5"></div>
    <label>性别:</label>
    <input type="text" name="staffGender" value="男" class="form-control" disabled>
    <!-- <input type="radio" name="staffGender" value="male" checked disabled> 男
    <input type="radio" name="staffGender" value="female" disabled> 女 -->
    <div class="clear-5"></div>
    <label>年龄:</label>
    <input name="staffAge" type="text" class="form-control" value="30" disabled>
    <div class="clear-5"></div>
    <label>联系方式:</label>
    <input name="tel" type="text" class="form-control" value="12398712938" disabled>
    <div class="clear-5"></div>
    <label>身份证号:</label>
    <input name="idNo" type="text" class="form-control" value="460912375098123" disabled>
  </div>
  <!-- *****上传照片***** -->
  <div class="form-group inline-block mb-0 fl text-center" style="width:50%">
    <!-- The fileinput-button span is used to style the file input field as button -->
    <div class="clear"></div>
    <span class="btn fileinput-button" style="padding:0">
        <img id="staff-avatar" src="" alt="" width="150" height="200">
        <input id="fileupload" style="width:150px; height:200px" type="file" name="files[]" multiple>
         <!-- The global progress bar -->
        <div id="progress" class="progress display-none">
            <div class="progress-bar progress-bar-success"></div>
        </div>
    </span>
    <input class="display-none" type="text" name="file-name" value="">
   
    <!-- The container for the uploaded files -->
    <div id="files" class="files"></div>
  </div> 
  <div class="clear-10-divider"></div>
  <div class="form-group inline-block fl" style="width:50%">
    <label>部门:</label>
    <input type="text" name="department" class="form-control" value="安保部" disabled>
    <div class="clear-5"></div>
    <label>职位:</label>
    <input type="text" name="position" class="form-control" value="普通保安" disabled>
    <div class="clear-5"></div>
    <label>入职日期:</label>
    <!-- <input type="text" name="entryDateDate" class="form-control" value="2014-08-05" disabled> -->
    <div class="input-group date" id="entryDate" for="staff-manage-details" data-date-format="yyyy-MM-dd">
        <input class="form-control" name="entryDate" value="2014-08-05" type="text" rel="startDate" readonly>
        <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
    </div>
    <div class="clear-5"></div>
    <label>离职日期:</label>
    <div class="input-group date" id="leavingDate" for="staff-manage-details" data-date-format="yyyy-MM-dd">
        <input class="form-control" name="leavingDate" value="----" type="text" rel="startDate" readonly>
        <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
    </div>
    <div class="clear-5"></div>
    <label>备注:</label>
    <input type="text" name="staffRemark" class="form-control" value="人不错" disabled>
  </div>
  <div class="form-group inline-block fl" style="width:50%">
    <label>状态:</label>
    <select name="staffStatus" class="form-control" disabled>
      <option value="1">在职</option>
      <option value="1">离职</option>
    </select>
  </div>
  <div class="clear-10"></div>
  <div class="form-group text-center buttonDetailsActions">
  </div>
</form>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="assets/js/jquery.ui.widget.js"></script>
<script src="assets/js/jquery.fileupload.js"></script>
<script src="assets/js/jquery.fileupload-process.js"></script>
<script src="assets/js/layer.js"></script>
<script src="assets/js/staff-manage-action.js"></script>
