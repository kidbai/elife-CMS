<div class="content-header clear">
    <h3 class="heading pull-left">
        <i class="fa fa-wrench"></i>
        推送消息
    </h3>
    <div class="operation-button pull-right">
    </div>
</div>
<div class="container-fluid">
    <div class="toolbar"></div>
    <table id="push-msg" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>序号</th>
                <th>推送内容(不超过70个字)</th>
                <th>短信(目标数量)</th>
                <th>客户端</th>
                <th>推送时间</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>结婚证办理资料和流程</td>
                <td>1000</td>
                <td>1000</td>
                <td>2015-08-29 08：55</td>
                <td>
                    <button class="btn btn-info" for="push-msg-search">查询</button>
                </td>
            </tr>
            <tr>
                <td>1</td>
                <td>结婚证办理资料和流程</td>
                <td>1000</td>
                <td>1000</td>
                <td>2015-08-29 08：55</td>
                <td>
                    <a href="push_msg/push_msg_search" class="pjaxTrigger btn btn-info">查询</a>
                </td>
            </tr>
            <tr>
                <td>1</td>
                <td>结婚证办理资料和流程</td>
                <td>1000</td>
                <td>1000</td>
                <td>2015-08-29 08：55</td>
                <td>
                    <a href="push_msg/push_msg_search" class="pjaxTrigger btn btn-info">查询</a>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<script src="assets/js/layer.js"></script>
<script src="assets/js/push-msg.js"></script>
