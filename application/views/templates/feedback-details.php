<form class="form-inline label-90 dialog" style="padding:30px 70px 30px 70px;">
    <div class="form-group inline-block fl" style="width:50%">
        <label>房号:</label>
        <div class="inline-block">1-1-101</div>
        <div class="clear"></div>
        <label>用户:</label>
        <div class="inline-block">张三</div>
    </div>
    <div class="form-group inline-block fl" style="width:50%">
        <label>用户编号:</label>
        <div class="inline-block">CD41234123</div>
        <div class="clear"></div>
        <label>用户电话:</label>
        <div class="inline-block">412351234</div>
    </div>
    <div class="clear"></div>
    <div class="form-group inline-block">
        <label>反馈建议:</label>
        <div class="inline-block">天气太热，小心中暑</div>
    </div>
    <div class="clear"></div>
    <div class="form-group inline-block">
        <div id="feedback-img">
            <a href="assets/img/avatar2.jpg" title='报修图片1'>
                <img src="assets/img/avatar2.jpg" for="public-monitoring-img" width="100" height="100">
            </a>
            <a href="assets/img/avatar.png" title='报修图片2'>
                <img src="assets/img/avatar.png" for="public-monitoring-img" width="100" height="100">
            </a>
            <a href="assets/img/avatar2.jpg" title='报修图片3'>
                <img src="assets/img/avatar2.jpg" for="public-monitoring-img" width="100" height="100">
            </a>
        </div> 
    </div>
    <div class="clear"></div>
    <div class="form-group inline-block">
        <label>提交时间:</label>
        <div class="inline-block">2015-07-12 12:18:56</div>
    </div>
    <div class="clear-10-divider"></div>
    <div class="form-group inline-block">
        <div class="reply-block">
            <div class="user fl">物业:</div>
            <div class="content fl">你们的建议我们已经收到了</div>
        </div>
        <div class="reply-block">
            <div class="user fl">用户 :</div>
            <div class="content fl">能不能搞快，要死了</div>
        </div>
        <div class="reply-block">
            <div class="user fl">物业:</div>
            <div class="content fl">领导说交钱才开空调</div>
        </div>
    </div>
    <div class="clear-10-divider"></div>
    <div class="form-group inline-block">
        <label>物业回复:</label>
        <textarea name="reply" style="text-indent:2em; width:270px; border: 1px solid #ccc;" placeholder="可不填写"></textarea>
    </div>
    <div class="form-group inline-block width-100 text-center">
        <button class="btn btn-info">确认</button>
    </div>
</form>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="assets/js/jquery.ui.widget.js"></script>
<script src="assets/js/jquery.fileupload.js"></script>
<script src="assets/js/jquery.fileupload-process.js"></script>
<script src="assets/js/layer.js"></script>
<script src="assets/js/blueimp-gallery.min.js"></script>
<script src="assets/js/feedback-action.js"></script>
<div id="blueimp-gallery" class="blueimp-gallery">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
</div>