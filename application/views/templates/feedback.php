<div class="content-header clear">
    <h3 class="heading pull-left">
        <i class="fa fa-wrench"></i>
        反馈建议
    </h3>
</div>
<div class="container-fluid">
    <div class="toolbar"></div>
    <table id="feedback" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>序号</th>
                <th>用户编号</th>
                <th>房号</th>
                <th>用户</th>
                <th>用户电话</th>
                <th>最新时间</th>
                <th>最新回复</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>CD412389751</td>
                <td>1-1-101</td>
                <td>张三</td>
                <td>1384712984</td>
                <td>2015-01-01 12：18</td>
                <td>用户：多少钱</td>
                <td>
                    <button class="btn btn-info" for="feedback-details">查询</button>
                </td>
            </tr>
            <tr>
                <td>1</td>
                <td>CD412389751</td>
                <td>1-1-101</td>
                <td>张三</td>
                <td>1384712984</td>
                <td>2015-01-01 12：18</td>
                <td>用户：多少钱</td>
                <td>
                    <button class="btn btn-info" for="feedback-details">查询</button>
                </td>
            </tr>
            <tr>
                <td>1</td>
                <td>CD412389751</td>
                <td>1-1-101</td>
                <td>张三</td>
                <td>1384712984</td>
                <td>2015-01-01 12：18</td>
                <td>用户：多少钱</td>
                <td>
                    <button class="btn btn-info" for="feedback-details">查询</button>
                </td>
            </tr>
            <tr>
                <td>1</td>
                <td>CD412389751</td>
                <td>1-1-101</td>
                <td>张三</td>
                <td>1384712984</td>
                <td>2015-01-01 12：18</td>
                <td>用户：多少钱</td>
                <td>
                    <button class="btn btn-info" for="feedback-details">查询</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<script src="assets/js/layer.js"></script>
<script src="assets/js/feedback.js"></script>
