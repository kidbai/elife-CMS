<form class="form-inline label-90 dialog">
  <div class="form-group">
    <label>车位号:</label>
    <input name="carportNo" type="text" class="form-control">
  </div>
  <div class="form-group">
    <label>车位状态:</label>
    <select name="carportStatus" class="form-control">
      <option value="xianzhi">闲置</option>
      <option value="zulin">租赁</option>
      <option value="出售">出售</option>
    </select>
  </div>
  <div class="form-group">
    <label>车辆编号:</label>
    <input name="carNo" type="text" class="form-control">
  </div>
  <div class="clear"></div>
  <div class="form-group">
    <label>用户编号:</label>
    <input name="carOwnerNo" type="text" class="form-control">
    <button class="btn btn-info">确定</button>
  </div>
  <div class="form-group">
    <label>用户姓名:</label>
    <input name="OwnerName" type="text" class="form-control" readonly>
  </div>
  <div class="clear"></div>
  <div class="form-group">
    <label>联系方式:</label>
    <input name="tel" type="text" class="form-control" readonly>
  </div>
  <div class="clear"></div>
  <div class="form-group">
    <label>备注:</label>
   <textarea name="remark" class="form-control"></textarea>
  </div>
  <div class="clear-10"></div>
  <div class="form-group text-center">
    <button type="button" class="btn btn-success" for="carport-manage-add">提交</button>
  </div>
</form>
<script src="assets/js/layer.js"></script>
<script src="assets/js/carport-manage-action.js"></script>
