<form class="form-inline label-90 dialog">
  <div class="form-group inline-block">
    <label>推送内容:</label>
    <textarea name="push-msg-content" style="text-indent:2em; width:270px; border: 1px solid #ccc;">推送内容</textarea>
    <div class="clear-5"></div>
    <label>推送方式:</label>
    <input name="push-way" type="checkbox" value="msg"> 短信
    <input name="push-way" type="checkbox" value="app"> 客户端
    <div class="clear-5"></div>
    <label>推送范围:</label>
    <input type="radio" name="push-area" value="all"> 所有业主
    <input type="radio" name="push-area" value="3eUsers"> 所有3E用户
    <input type="radio" name="push-area" value="allStaff"> 所有员工
    <div class="clear-5"></div>
    <label></label>
    <input type="radio" name="push-area" value="phoneNo"> 电话号码
    <div class="relative mt-5">
        <div class="clear"></div>
        <label></label>
        <input type="text" name="phoneNo" class="form-control" placeholder=" 输入手机号码" for="phoneNo">
    </div>
    <div class="clear"></div>
  </div>
  <div class="clear-10"></div>
  <div class="form-group text-center">
    <button type="button" class="btn btn-info" for="phoneNo">新增号码</button>
    <button type="button" class="btn btn-success" for="push-msg-submit">确认</button>
  </div>
</form>
<script src="assets/js/push-msg-action.js"></script>
