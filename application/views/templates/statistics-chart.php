<div class="content-header clear">
    <h3 class="heading pull-left">
        <i class="fa fa-wrench"></i>
        统计分析 
    </h3>
    <div class="operation-button pull-right">
        <a href="home/statistics" class="btn btn-default pjaxTrigger">表格统计</a>
        <a href="home/statistics_chart" class="btn btn-danger pjaxTrigger">图形统计</a>
    </div>
</div>

<div id="chart-1" class="chart"></div>
<div id="chart-2" class="chart"></div>
<div id="chart-3" class="chart"></div>

<script src="assets/echarts/echarts.js"></script>
<script src="assets/js/statistics-chart.js"></script>