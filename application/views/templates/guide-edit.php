<div class="wrapper-edit">
    <div class="container-900">
        <div class="page">
            <form>
                <header class="guide-header">
                    <textarea name="doc_title" placeholder="文档标题" ></textarea>
                </header>

                <textarea name="doc_content" id="doc_content"></textarea>
                
                <div class="clear-10"></div>
                <div class="form-group text-center">
                    <button type="button" class="btn btn-default ml-10" for="guide-cancel">取消</button>
                    <button type="button" class="btn btn-primary ml-10" for="guide-submit">发布</button>
                    <!-- <button type="button" class="btn btn-info ml-10" for="community-news-preview">预览</button> -->
                </div>    
            </form>
        </div>
    </div>
    
</div>


<script src="assets/js/jquery.ui.widget.js"></script>
<script src="assets/js/jquery.fileupload.js"></script>
<script src="assets/js/jquery.fileupload-process.js"></script>
<script src="assets/js/jquery.hotkeys.js"></script>
<script src="assets/js/kindeditor-all-min.js"></script>
<script src="assets/js/zh-CN.js"></script>
<script src="assets/js/guide-action.js"></script>
