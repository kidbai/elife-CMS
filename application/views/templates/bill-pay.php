<form class="form-inline dialog text-center">
  <div class="form-group fl">
    <label>房号:</label>
    <div class="inline-block">1-1-101</div>
  </div>
  <div class="form-group">
    <label>业主:</label>
    <div class="inline-block">李华成|张子佳</div>
  </div>
  <div class="form-group fr">
    <label>收费日期:</label>
    <div class="inline-block">2015-07-22</div>
  </div>
  <div class="clear"></div>
  <table class="table table-bordered mt-10">
    <thead>
      <tr>
        <th>费用类型</th>
        <th>收费标准(元/月)</th>
        <th>数量(月)</th>
        <th>费用到期日期</th>
        <th>小计(元)</th>
        <th>总计(元)</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>物业费</td>
        <td for="unit-price">￥799.00</td>
        <td for="count">
          <select name="month">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
          </select>
        </td>
        <td>2015-01-01</td>
        <td for="subtotal">￥799</td>
        <td rowspan="3" for="bill-total">1300元</td>
      </tr>
      <tr>
        <td>清洁费</td>
        <td for="unit-price">￥799.00</td>
        <td for="count">
          <select name="month">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
          </select>
        </td>
        <td>2015-01-01</td>
        <td for="subtotal">￥799</td>
      </tr>
      <tr>
        <td>停车费</td>
        <td for="unit-price">￥799.00</td>
        <td for="count">
          <select name="month">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
          </select>
        </td>
        <td>2015-01-01</td>
        <td for="subtotal">￥799</td>
      </tr>
    </tbody>
  </table>
  <div class="clear"></div>
  <div class="form-group">
    <label>物业客服电话:</label>
    <div class="inline-block">XXX-XXXXXXXX</div>
  </div>
  <div class="form-group">
    <label>缴款人:</label>
    <input type="text" class="form-control" name="payer">
  </div>
  <div class="form-group">
    <label>收款人</label>
    <input type="text" class="form-control" name="payee" value="张三">
  </div>
  <div class="clear-10-divider"></div>
  <div class="form-group">
    <input type="checkbox" name="print" checked>
    打印收据单 
  </div>
  <div class="clear-10"></div>
  <div class="form-group text-center">
      <button type="button" class="btn btn-primary" for="bill-pay">确认</button>
  </div>
</form>
<script src="assets/js/layer.js"></script>
<script src="assets/js/bill-pay-action.js"></script>
