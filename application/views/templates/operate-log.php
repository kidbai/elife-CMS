<div class="content-header clear">
    <h3 class="heading pull-left">
        <i class="fa fa-wrench"></i>
        操作日志 
    </h3>
    <div class="operation-button pull-right">
    </div>
</div>
<div class="container-fluid">
    <div class="toolbar"></div>
    <table id="operate-log" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>序号</th>
                <th>来源IP</th>
                <th>操作描述</th>
                <th>操作员</th>
                <th>操作时间</th>
                <th>操作结果</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td></td>
                <td>1</td>
                <td>192.168.1.1</td>
                <td>查询用户信息</td>
                <td>kefu1</td>
                <td>2015-10-01 07：25</td>
                <td>成功</td>
            </tr>
            <tr>
                <td></td>
                <td>1</td>
                <td>192.168.1.1</td>
                <td>查询用户信息</td>
                <td>kefu1</td>
                <td>2015-10-01 07：25</td>
                <td>成功</td>
            </tr>
            <tr>
                <td></td>
                <td>1</td>
                <td>192.168.1.1</td>
                <td>查询用户信息</td>
                <td>kefu1</td>
                <td>2015-10-01 07：25</td>
                <td>成功</td>
            </tr>
        </tbody>
    </table>
</div>
<script src="assets/js/layer.js"></script>
<script src="assets/js/operate-log.js"></script>
