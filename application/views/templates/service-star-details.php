<header class="service-header form-group">
    服务之星监督详情 
</header>
<div class="service-info">
    <div class="form-group service-info-block">
        <label>工号:</label>
        <div class="inline-block">CD378987899</div>
        <div class="clear"></div>
        <label>姓名:</label>
        <div class="inline-block">张三</div>
        <div class="clear"></div>
        <label>性别:</label>
        <div class="inline-block">男</div>
        <div class="clear"></div>
        <label>年龄:</label>
        <div class="inline-block">36</div>
    </div>
    <div class="form-group service-info-block">
        <label>部门:</label>
        <div class="inline-block">保安部</div>
        <div class="clear"></div>
        <label>职位:</label>
        <div class="inline-block">普通保安</div>
        <div class="clear"></div>
        <label>联系方式:</label>
        <div class="inline-block">13812489124</div>
    </div>
    <div class="form-group service-info-block">
        <label>本月赞:</label>
        <div class="inline-block">8</div>
        <div class="clear"></div>
        <label>评论:</label>
        <div class="inline-block">8</div>
        <div class="clear"></div>
        <label>总赞:</label>
        <div class="inline-block">13</div>
    </div>
    <div class="form-group service-info-block">
        <img src="assets/img/avatar2.jpg" alt="">
    </div>
</div>
<div class="padding-30">
    <table id="service-star-details" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>用户编号</th>
                <th>房号</th>
                <th>姓名</th>
                <th>联系方式</th>
                <th>赞</th>
                <th>评论</th>
                <th>提交时间</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>CD1294871284</td>
                <td>1-2-191</td>
                <td>张三</td>
                <td>318319844</td>
                <td>+1</td>
                <td>哎哎可减肥哈斯卡收到</td>
                <td>2015-07-21 09：45</td>
            </tr>
            <tr>
                <td>CD1294871284</td>
                <td>1-2-191</td>
                <td>张三</td>
                <td>318319844</td>
                <td>+1</td>
                <td>哎哎可减肥哈斯卡收到</td>
                <td>2015-07-21 09：45</td>
            </tr>
            <tr>
                <td>CD1294871284</td>
                <td>1-2-191</td>
                <td>张三</td>
                <td>318319844</td>
                <td>+1</td>
                <td>哎哎可减肥哈斯卡收到</td>
                <td>2015-07-21 09：45</td>
            </tr>
            <tr>
                <td>CD1294871284</td>
                <td>1-2-191</td>
                <td>张三</td>
                <td>318319844</td>
                <td>+1</td>
                <td>哎哎可减肥哈斯卡收到</td>
                <td>2015-07-21 09：45</td>
            </tr>
        </tbody>
    </table>
</div>


<script src="assets/js/service-star-details.js"></script>
