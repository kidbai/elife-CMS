<form class="form-inline dialog">
  <div class="form-group">
    <label>账单类型:</label>
    <input name="billType" type="text" class="form-control">
  </div>
  <div class="form-group">
    <label class="contron-label">账单生成周期:</label>
    <select name="billPeriod—1" class="form-control ml-5 mr-5 width-50">
      <option value="month">每月</option>
      <option value="year">每年</option>
    </select>
    <select name="billPeriod—2" class="form-control ml-5 mr-5 width-50">
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
      <option value="8">8</option>
    </select>
  </div>
  <div class="form-group">
    <label>收费标准:</label>
    <input name="billStandard" type="text" class="form-control">
    <select name="billUnit" class="form-control ml-5 mr-5 width-50">
      <option value="squareMeter">元/m2</option>
      <option value="squareCentimetre">元/cm2</option>
    </select>
  </div>
  <div class="clear"></div>
  <div class="form-group">
    <label>备注:</label>
    <textarea name="remark" class="form-control"></textarea>
  </div>
  <div class="clear-10"></div>
  <div class="form-group text-center">
    <button type="button" class="btn btn-success" for="bill-rule-add">提交</button>
  </div>
</form>
<script src="assets/js/layer.js"></script>
<script src="assets/js/bill-rule-action.js"></script>
