<div class="content-header clear">
    <h3 class="heading pull-left">
        <i class="fa fa-wrench"></i>
        访客通行
    </h3>
    <div class="operation-button pull-right"></div>
</div>
<div class="container-fluid">
    <div class="toolbar"></div>
    <table id="visitor" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>用户编号</th>
                <th>房号</th>
                <th>姓名</th>
                <th>提交时间</th>
                <th>到访时间</th>
                <th>访客姓名</th>
                <th>访客电话</th>
                <th>状态</th>
                <th>备注</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>CD123134123</td>
                <td>1-1-101</td>
                <td>张三</td>
                <td>2015-07-09 12：12：12</td>
                <td>今天 07：00 - 09：00</td>
                <td>张四</td>
                <td>1388098094</td>
                <td>已提交</td>
                <td>携带食品</td>
            </tr>
            <tr>
                <td>CD123134123</td>
                <td>1-1-101</td>
                <td>张三</td>
                <td>2015-07-09 12：12：12</td>
                <td>今天 07：00 - 09：00</td>
                <td>张四</td>
                <td>1388098094</td>
                <td>已提交</td>
                <td>携带食品</td>
            </tr>
            <tr>
                <td>CD123134123</td>
                <td>1-1-101</td>
                <td>张三</td>
                <td>2015-07-09 12：12：12</td>
                <td>今天 07：00 - 09：00</td>
                <td>张四</td>
                <td>1388098094</td>
                <td>已提交</td>
                <td>携带食品</td>
            </tr>
            <tr>
                <td>CD123134123</td>
                <td>1-1-101</td>
                <td>张三</td>
                <td>2015-07-09 12：12：12</td>
                <td>今天 07：00 - 09：00</td>
                <td>张四</td>
                <td>1388098094</td>
                <td>已提交</td>
                <td>携带食品</td>
            </tr>
        </tbody>
    </table>
</div>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="assets/js/bootstrap-dialog.js"></script>
<script src="assets/js/layer.js"></script>
<script src="assets/js/visitor.js"></script>
