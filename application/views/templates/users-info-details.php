<form class="form-inline dialog" style="padding:30px 70px 30px 70px;">

  <div class="form-group inline-block fl" style="width:50%">
    <label>用户编号:</label>
    <div class="inline-block">CD912837123</div>
    <div class="clear-5"></div>
    <label>姓名:</label>
    <div class="inline-block">张三</div>
    <div class="clear-5"></div>
    <label>性别:</label>
    <div class="inline-block">男</div>
    <div class="clear-5"></div>
    <label>联系方式:</label>
    <div class="inline-block">12398712938</div>
    <div class="clear-5"></div>
    <label>用户角色:</label>
    <div class="inline-block">租户</div>
    <div class="clear-5"></div>
    <label>用户状态:</label>
    <div class="inline-block">激活</div>
  </div>
  <div class="form-group inline-block mb-0 fl text-center" style="width:50%">
    <label>房号:</label>
    <div class="inline-block">1-2-1</div>
    <div class="clear-5"></div>
    <label>建筑面积:</label>
    <div class="inline-block">102m2</div>
    <div class="clear-5"></div>
    <label>车位号:</label>
    <div class="inline-block">A 101</div>
    <div class="clear-5"></div>
    <label>车位状态:</label>
    <div class="inline-block">租赁</div>
    <div class="clear-5"></div>
    <label>车牌号码:</label>
    <div class="inline-block">川A 1rar1</div>
    <div class="clear-5"></div>
    <label>车位备注:</label>
    <div class="inline-block">阿萨德发生地方啊 </div>
  </div> 
  <div class="clear"></div>
  <div class="form-group inline-block">
    <label>用户备注:</label>
    <div class="inline-block">adsasdf</div>
    <div class="clear-5"></div>
    <label>注册时间:</label>
    <div class="inline-block">2015-07-22 10:14</div>
    <div class="clear-5"></div>
    <label>注销时间:</label>
    <div class="inline-block">2015-07-22 10:14</div>
  </div>
  <div class="clear-10"></div>
  <div class="form-group text-center width-100">
    <button type="button" class="btn btn-warning" for="users-info-logoff">注销</button>
  </div>
</form>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="assets/js/jquery.ui.widget.js"></script>
<script src="assets/js/jquery.fileupload.js"></script>
<script src="assets/js/jquery.fileupload-process.js"></script>
<script src="assets/js/layer.js"></script>
<script src="assets/js/users-info-action.js"></script>
