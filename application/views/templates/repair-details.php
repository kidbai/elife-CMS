<form class="form-inline label-90 dialog" style="padding:30px 70px 30px 70px;">
    <div class="form-group inline-block fl" style="width:50%">
        <label>房号:</label>
        <div class="inline-block">1-1-1001</div>
        <div class="clear"></div>
        <label>报修人:</label>
        <div class="inline-block">张三</div>
        <div class="clear"></div>
        <label>报修人电话:</label>
        <div class="inline-block">131284689124</div>
        <div class="clear"></div>
    </div>
    <div class="form-group inline-block mb-0 fl text-center" style="width:50%">
        <label>报修单号:</label>
        <div class="inline-block">1231241234</div>
        <div class="clear"></div>
        <label>报修状态:</label>
        <div class="inline-block">待处理</div>
        <div class="clear"></div>
        <label>预约时间:</label>
        <div class="inline-block">A 101</div>
    </div> 
    <div class="clear"></div>
    <div class="form-group inline-block">
        <label>报修描述:</label>
        <div class="inline-block">家里厨房有问题</div>
    </div>
    <div class="clear"></div>
    <div class="form-group inline-block">
        <div id="links">
            <a href="assets/img/avatar2.jpg" title='报修图片1'>
                <img src="assets/img/avatar2.jpg" for="public-monitoring-img" width="100" height="100">
            </a>
            <a href="assets/img/avatar.png" title='报修图片2'>
                <img src="assets/img/avatar.png" for="public-monitoring-img" width="100" height="100">
            </a>
            <a href="assets/img/avatar2.jpg" title='报修图片3'>
                <img src="assets/img/avatar2.jpg" for="public-monitoring-img" width="100" height="100">
            </a>
            <a href="assets/img/avatar2.jpg" title='报修图片3'>
                <img src="assets/img/avatar2.jpg" for="public-monitoring-img" width="100" height="100">
            </a>
        </div> 
    </div>
    <div class="clear"></div>
    <div class="form-group inline-block">
        <label>提交时间:</label>
        <div class="inline-block">2015-07-12 12:18:56</div>
    </div>
    <div class="clear-10-divider"></div>
    <div class="form-group inline-block">
        <label>维修人分配:</label>
        <select name="maintainer" class="form-control">
            <option value="1">张三</option>
            <option value="2">张4</option>
            <option value="3">张5</option>
            <option value="4">张6</option>
        </select>
    </div>
    <div class="clear"></div>
    <div class="form-group inline-block">
        <label>修改预约时间:</label>
        <div class="input-group date" id="modifyAppointmentTime" data-date-format="yyyy-MM-dd - HH:ii P">
            <input class="form-control" name="appointmentTime" type="text" rel="startDate" readonly>
            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
            <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
        </div>
    </div>
    <div class="form-group inline-block">
        <label>物业回复:</label>
        <textarea name="reply" style="text-indent:2em; width:270px; border: 1px solid #ccc" placeholder="可不填写"></textarea>
    </div>
    <div class="form-group inline-block width-100 text-center">
        <button class="btn btn-danger">无效</button>
        <button class="btn btn-info ml-10">处理中</button>
    </div>
</form>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="assets/js/jquery.ui.widget.js"></script>
<script src="assets/js/jquery.fileupload.js"></script>
<script src="assets/js/jquery.fileupload-process.js"></script>
<script src="assets/js/layer.js"></script>
<script src="assets/js/blueimp-gallery.min.js"></script>
<script src="assets/js/repair-action.js"></script>
<div id="blueimp-gallery" class="blueimp-gallery">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
</div>