<div class="content-header clear">
    <h3 class="heading pull-left">
        <i class="fa fa-wrench"></i>
        操作员管理 
    </h3>
    <div class="operation-button pull-right">
    </div>
</div>
<div class="container-fluid">
    <div class="toolbar"></div>
    <table id="operator" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>序号</th>
                <th>角色</th>
                <th>账号</th>
                <th>工号</th>
                <th>姓名</th>
                <th>性别</th>
                <th>联系方式</th>
                <th>部门</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td></td>
                <td>1</td>
                <td>客服值班员</td>
                <td>kefu1</td>
                <td>CD87918274</td>
                <td>张三</td>
                <td>男</td>
                <td>156192847124</td>
                <td>客服部</td>
                <td>
                    <button class="btn btn-info" for="operator-edit">编辑</button>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>1</td>
                <td>客服值班员</td>
                <td>kefu1</td>
                <td>CD87918274</td>
                <td>张三</td>
                <td>男</td>
                <td>156192847124</td>
                <td>客服部</td>
                <td>
                    <button class="btn btn-info" for="operator-edit">编辑</button>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>1</td>
                <td>客服值班员</td>
                <td>kefu1</td>
                <td>CD87918274</td>
                <td>张三</td>
                <td>男</td>
                <td>156192847124</td>
                <td>客服部</td>
                <td>
                    <button class="btn btn-info" for="operator-edit">编辑</button>
                </td>
            </tr>
            
        </tbody>
    </table>
</div>
<script src="assets/js/layer.js"></script>
<script src="assets/js/operator.js"></script>
