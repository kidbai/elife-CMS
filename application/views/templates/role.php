<div class="content-header clear">
    <h3 class="heading pull-left">
        <i class="fa fa-wrench"></i>
        角色管理
    </h3>
    <div class="operation-button pull-right">
    </div>
</div>
<div class="container-fluid">
    <div class="toolbar"></div>
    <table id="role" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>序号</th>
                <th>角色名称</th>
                <th>角色备注</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td></td>
                <td>1</td>
                <td>客服值班员</td>
                <td>负责物业缴费、社区服务</td>
                <td>
                    <button class="btn btn-info" for="role-edit">查询</button>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>1</td>
                <td>信息员</td>
                <td>便民信息</td>
                <td>
                    <button class="btn btn-info" for="role-edit">查询</button>
                </td>
            </tr>
            
        </tbody>
    </table>
</div>
<script src="assets/js/layer.js"></script>
<script src="assets/js/role.js"></script>
