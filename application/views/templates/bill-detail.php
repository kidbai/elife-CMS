<div class="content-header clear">
    <h3 class="heading pull-left">
        <i class="fa fa-wrench"></i>
        缴费详情
    </h3>
    <div class="operation-button pull-right"></div>
</div>
<div class="container-fluid">
    <div class="toolbar"></div>
    <table id="bill-detail" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>订单号</th>
                <th>房号</th>
                <th>总计</th>
                <th>缴费途径</th>
                <th>缴费人</th>
                <th>缴费时间</th>
            </tr>
        </thead>
    </table>
</div>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="assets/js/bootstrap-dialog.js"></script>
<script src="assets/js/layer.js"></script>
<script src="assets/js/bill-detail.js"></script>
