<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<base href="<?=base_url()?>">

<link rel="stylesheet" href="assets/css/style.css" />
<link rel="stylesheet" href="assets/css/lib.css" />
<link rel="stylesheet" href="assets/css/jquery.dataTables.css" />
<link rel="stylesheet" href="assets/css/dataTables.responsive.css" />
<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
<link rel="stylesheet" href="assets/font-awesome-4.3.0 2/css/font-awesome.min.css" />
<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="assets/css/bootstrap-dialog.min.css">
<link rel="stylesheet" href="assets/css/jquery.fileupload.css">
<link rel="stylesheet" href="assets/css/jquery.fileupload-ui.css">
<link rel="stylesheet" href="assets/css/blueimp-gallery/blueimp-gallery.min.css">
<script type="text/javascript" src="assets/js/jquery-1.11.2.min.js"></script>
<script src="assets/js/common.js"></script>
