<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>PJAX DEMO</title>
        <base href="<?=base_url()?>">
        <link rel="stylesheet" href="assets/css/style.css" />
        <link rel="stylesheet" href="assets/css/lib.css" />
        <link rel="stylesheet" href="assets/css/jquery.dataTables.css" />
        <link rel="stylesheet" href="assets/css/dataTables.responsive.css" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/font-awesome-4.3.0 2/css/font-awesome.min.css" />
        <link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap-dialog.min.css">
        <script type="text/javascript" src="assets/js/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="assets/js/dataTables.responsive.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <button class="navbar-toggle pull-left collapsed" type="button" data-toggle="collapse" data-target="#sidebar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="" class="navbar-brand">
                        凤凰城1期
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right hidden-xs">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expended="false">
                            <i class="fa fa-exclamation-triangle fa-fw"></i>
                            <span class="badge badge-notification badge-info animated fateIn">3</span>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts d-timeline animated fadeInUp">
                            <li></li>
                            <li class="info">
                                <span class="circle"></span>
                                <span class="stacked-text">
                                    <i class="fa fa-comment fa-fw"></i>
                                    家用报修有<a href="">4条</a>新消息
                                    <small class="help-block text-muted">4分钟前</small>
                                </span>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle user" data-toggle="dropdown" aria-expended="false">
                            Youngbye
                            <img class="img-circle user-img" src="assets/img/avatar.png" width="30" height="30"/>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user animated fadeInUp">
                            <li>
                                <a href="#">
                                    <i class="fa fa-info-circle"></i>
                                    我的信息
                                </a>
                            </li>
                            <li>
                               <a href="#">
                                   <i class="fa fa-gear fa-fw"></i>
                                   修改密码
                               </a> 
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#" class="text-danger">
                                    <i class="fa fa-sign-out fa-fw"></i>
                                    退出
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <nav id="menu" class="navbar-default navbar-fixed-side" role="navigation" tabindex="5000">
                <div class="sidebar-collapse collapse navbar-collapse" id="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <!-- <li class="sidebar-user">
                            <div class="user-img">
                                <img class="img-circle" width="65" height="65" src="assets/img/avatar.png" alt="" />
                            </div>
                            <div class="user-info">
                                <div class="user-greet">欢迎使用</div>
                                <div class="user-name">杨柏</div>
                            </div>
                        </li> -->
                        
                        <!-- <li position="0">
                            <a href="home" class="pjaxTrigger">
                                <i class="fa fa-desktop fa-fw"></i>
                                主界面
                            </a>
                        </li> -->
                        <li position="1">
                            <a href="">
                                <i class="fa fa-money fa-fw"></i>
                                物业缴费
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="nav nav-second-level collapse">
                                <li><a class="pjaxTrigger" href="home/resident_bill" position="0">住户账单</a></li>
                                <li><a class="pjaxTrigger" href="home/bill_detail" position="1">缴费详情</a></li>
                            </ul>
                        </li>
                        <li position="2">
                            <a href="">
                                <i class="fa fa-cart-plus fa-fw"></i>
                                社区服务
                                <i class="fa fa-angle-down"></i>
                            </a>
                             <ul class="nav nav-second-level">
                                <li><a class="pjaxTrigger" href="home/users_info" position='0'>用户信息</a></li>
                                <li><a class="pjaxTrigger" href="home/repair" position='1'>家用报修</a></li>
                                <li><a class="pjaxTrigger" href="home/visitor" position='2'>访客通行</a></li>
                                <li><a class="pjaxTrigger" href="home/public_monitoring" position='3'>公共监察</a></li>
                                <li><a class="pjaxTrigger" href="home/feedback" position='4'>反馈建议</a></li>
                            </ul>
                        </li>
                        <li position="3">
                            <a href="">
                                <i class="fa fa-user-plus fa-fw"></i>
                                便民信息
                                <i class="fa fa-angle-down"></i>
                            </a>
                             <ul class="nav nav-second-level">
                                <li><a class="pjaxTrigger" href="home/community_news" position='0'>小区动态</a></li>
                                <li><a class="pjaxTrigger" href="home/guide" position='1'>办事指南</a></li>
                                <li><a class="pjaxTrigger" href="home/push_msg" position='2'>消息推送</a></li>
                            </ul>
                        </li>
                        <li position="4">
                            <a href="">
                                <i class="fa fa-users fa-fw"></i>
                                小区管理
                                <i class="fa fa-angle-down"></i>
                            </a>
                             <ul class="nav nav-second-level">
                                <li><a class="pjaxTrigger" href="home/service" position='0'>服务监督</a></li>
                                <li><a class="pjaxTrigger" href="home/statistics" position='1'>统计分析</a></li>
                                <li><a class="pjaxTrigger" href="home/bill_rule" position='2'>账单规则</a></li>
                                <li><a class="pjaxTrigger" href="home/owner_manage" position='3'>业主管理</a></li>
                                <li><a class="pjaxTrigger" href="home/carport_manage" position='4'>车位管理</a></li>
                                <li><a class="pjaxTrigger" href="home/staff_manage" position='5'>员工管理</a></li>
                                <li><a class="pjaxTrigger" href="home/department_manage" position='6'>部门管理</a></li>
                            </ul>
                        </li>
                        <li position="5">
                            <a href="">
                                <i class="fa fa-gears fa-fw"></i>
                                系统配置
                                <i class="fa fa-angle-down"></i>
                            </a>
                             <ul class="nav nav-second-level">
                                <li><a class="pjaxTrigger" href="home/role" position='0'>角色管理</a></li>
                                <li><a class="pjaxTrigger" href="home/operator" position='1'>操作员管理</a></li>
                                <li><a class="pjaxTrigger" href="home/operate_log" position='2'>操作日记</a></li>
                            </ul>
                        </li>
                        <li>
                            <div class="weather-wrapper">
                                <div class="weather-card">
                                    <div class="weather-icon sun"></div>
                                    <!-- <div class="weather-icon cloud"></div> -->
                                    <h2>30º</h2>
                                    <p>成都</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
            <div id="content-wrapper" class="fixed-navbar">
                <? echo $TEMPLATE_CONTENT;?>
            </div>
        </div>
        </p>

        <script type="text/javascript" src="assets/js/jquery-pjax.js"></script>
        <script type="text/javascript" src="assets/js/common.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
     </body>
</html>